# LiberaForms

https://www.liberaforms.org

See the `/docs` directory for installation and setup documentation.

## What is LiberaForms

LiberaForms is an easy to use software for creating and managing simple web Forms and
their Answers (collected data).

This software is licensed under the AGPLv3. We hope you find it useful.

### User features include

* Easy to use Form creation (usual form elements. input, textarea, date, etc.)
* Form styling
* Solicit file attachments
* Email notifications
* Form expiration conditionals
* Editable 'Thank you for submitting' texts
* Editable 'This form has expired' texts
* Form GDPR statments and templating
* GDPR statement wizard
* Answers edition and edition history log
* Export answers in Graphs, CSV, JSON, PDF
* Share Form edition with other users
* Share Answers (read-only) with other users
* Anonymous user Answer edition
* Usage statistics
* Publish Forms on the Fediverse
* Form templates
* Inline help

### Admin features include

* Site configuration parameters
* List all Forms, view and manage forms
* List all Users, view and manage users
* Per user and site-wide usage statistics
* Custom tranlations for texts and interface components
* Inline help

### Sysadmin

Keywords. `Python` `Flask` `PostgreSQL` `Nginx` `VueJS` `RSS` `Docker` `Prometheus` `LDAP`, `JWT`, `Woodpecker`, `pytest`

Releases are anounced on the blog https://blog.liberaforms.org

See `./docs` for more info. :)
