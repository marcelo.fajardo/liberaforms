# Etikaj formularoj kun LiberaForms

[LiberaForms](https://liberaforms.org/eo) estas ilo de libera programaro pensita kaj programita kiel **komuna infrastrukturo, libera kaj etika** kiu ebligas krei kaj administri formularojn kiuj respektas la ciferecajn rajtojn de homoj kiuj uzas ĝin.

Kun LiberaForms vi povas konsulti, modifi kaj elŝuti la respondojn ricevitajn; inkludi markobutonon por peti konsenton pri la Leĝo de Protektado de Datumoj; kunlabori kun aliaj uzantoj per komunaj permesoj; kaj multaj aferoj pli! Krome, LiberaForms estas liberkulturo sub licenco AGPLv3.

Uzu, konigi kaj plibonigi ĝin!
