# Galdetegi etikoak LiberaForms-ekin

[LiberaForms](https://liberaforms.org/eu) software libreko tresna bat da, **azpiegitura komunitario, libre eta etiko** gisa pentsatu eta garatua, erabiltzen duten pertsonen eskubide digitalak errespetatzen dituzten galdetegiak sortu eta kudeatzeko aukera ematen duena.

LiberaForms-ekin zure galdetegietan jasotako erantzunak kontsultatu, editatu eta deskargatu ditzakezu; Datu Pertsonalen Babeserako Legea onartzeko kontrol-lauki bat gehitu; beste erabiltzaile batzuekin elkarlanean aritu zaitezke, behin baimenak partekatuta; eta askoz gauza gehiago egin ahal izango dituzu! Gainera LiberaForms AGPLv3 lizentziapeko kultura librea da.

Erabili, partekatu eta hobetu ezazu!
