

{{ _("People will read this text when they visit %(index_url)s", index_url="<a href='BASE_URL'>BASE_URL</a>"|replace('BASE_URL', BASE_URL)) }}

{% if is_multilanguage_site %}
  {% include './_translation_option.j2.md' %}
{% endif %}

---

[{{ _("Edit the text") }}<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>]({{url_for('site_bp.edit_blurb')}})
