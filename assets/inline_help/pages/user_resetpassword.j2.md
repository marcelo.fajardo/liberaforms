

{{ _("A strong password is important to protect your account and the data you collect.") }}

{{ _("Your password must contain special characters.") }}

## {{ _("Our recommendation") }}

{{ _("We suggest you use a passphrase, a sentence. Think of something dear to you.") }}

{{ _("Example: Peter cooks delicious food") }}

{{ _("In this case `P` is a capital letter and the spaces between words are special characters.") }}
