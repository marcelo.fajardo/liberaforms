

{{ _("A list of privacy statements created by the Admins of this site.") }}

{{ _("Statements can be:") }}

+ {{ _("Attached to the New user form such as a 'Terms and conditions' policy") }}
+ {{ _("Shared with the users on your site") }}

{{ with_link(_("Learn how to $$add a statement$$ to the library."), "site_dataconsent_edit.j2.md") }}

## {{ _("Privacy statements") }}

{{ _("Each statement on this page displays:") }}

+ {{ _("The forms the statement is attached to") }}
+ {{ _("The forms that have used the statement as a template") }}
+ {{ _("Agreement is required") }}<span class="ds-required" arial-label="required"></span>
+ {{ _("The date the statement was created") }}

## {{ _("Editing and deleting privacy statements") }}

{% include './_editing_and_deleted_consent_templates.j2.md' %}

---
[{{_("Site privacy statement library")}}<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>]({{url_for('consent_bp.list_site_data_consents')}})
