

## {{ _("General") }}

+ {{ help_page(label=_("Email address"), file_name="user_changeemail.j2.md") }}: {{ _("Enter an address and you will receive a confirmation email") }}
+ {{ help_page(label=_("Password"), file_name="user_resetpassword.j2.md") }}: {{ _("Change your password") }}
+ {{ help_page(label=_("Avatar"), file_name="user_avatar.j2.md") }}: {{ _("Set your avatar") }}
+ {{ help_page(label=_("Language"), file_name="user_changelanguage.j2.md") }}: {{ _("Your preferred language") }}
+ {{ help_page(label=_("Time zone"), file_name="user_changetimezone.j2.md") }}: {{ _("Time zone") }}
+ {{ help_page(label=_("Delete my account"), file_name="user_deleteaccount.j2.md") }}: {{ _("All your forms, answers and media will be deleted") }}

## {{ _("Form notifications") }}

{{ _("Be notified when your forms receive a new answer.") }}

{{ with_link(_("This is the default setting for your new forms and can be changed for each individual form on the form's $$options page$$."), "form_options.j2.md") }}

## {{ _("File uploads") }}

{{ _("To keep your used storage space to a minimun you can:") }}

+ {{ _("Delete unused media files") }}
+ {{ _("Delete old form attachments") }}

{{ with_link(_("$$Add and delete$$ your media files."), "user_media.j2.md") }}

{% if current_user.can_configure_fediverse() %}
## {{ _("Social media") }}

{{ with_link(_("Configure this option to publish your forms on the $$Fediverse$$."), "user_fediverse.j2.md") }}
{% endif %}

{% if is_admin %}
## {{ _("My admin settings") }}

+ {{ _("Notify me when new user has registered") }}
+ {{ _("Notify me when new form has been created") }}
{% endif %}

{% if site.get_contact_info() %}
## {{ _("Other information") }}

{{ _("The administrators of this site have written this text for you.") }}
{% endif %}

---
[{{ _("My profile settings") }}<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>]({{url_for('user_bp.user_settings')}})
