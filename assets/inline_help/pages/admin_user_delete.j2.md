

{{ _("Permanently delete a user, the forms they have created and those forms' answers.") }}

## {{ _("This user's forms") }}

{{ _("A list of the forms created by this user:") }}

+ {{ _("Slug") }}: {{ _("The form's slug") }}
+ {{ _("Editors") }}: {{ _("The number of people the form is shared with") }}
+ {{ _("Answers") }}: {{ _("The number of answers submitted to the form") }}

{{ _("To confirm, enter the name of this user here.") }}

{{ _("This cannot be undone!") }}
