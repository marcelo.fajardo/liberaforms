

{{ _("The people who answer your forms implicitly entrust you with their information.") }}

{{ _("When you solicit a name, an email, a postal address, etc, you should tell your users why you are collecting their data and how you will use it.")}}

{{ _("You are responsible for the data you collect and how you process it.") }}

{% set link = i18n_docs_site_url("https://docs.liberaforms.org/user-guide/privacy-statements/") %}
{{with_link(_("See the $$documentation website$$ for more information."), link, external_page=True)}}

## {{ _("My privacy statement library") }}

{{ with_link(_("$$Privacy statements you create$$ can be used later as templates for your forms."), "user_dataconsent_edit.j2.md") }}
{{ with_link(_("The $$wizard$$ will help you do that."), "wizard.j2.md") }}

## {{ _("Using a template") }}

{{ with_link(_("After $$attaching a statement$$ to a form, you can edit it to fit."), "form_dataconsents.j2.md") }}

{% include './_editing_and_deleted_consent_templates.j2.md' %}

---

[{{_("My privacy statement library")}}<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>]({{url_for('consent_bp.list_user_data_consents')}})
