
{{ _("Permanently delete your account.") }}

{{ _("You will delete all of your forms and their answers")}}

{{ _("This is irreversible!") }}

## {{ _("Shared forms") }}

{{ _("Please note that the forms you have shared with other people are deleted.") }}
