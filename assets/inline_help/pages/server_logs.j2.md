
{{ _("Most recent server activity in chronological order.") }}

## {{ _("Server logs") }}

{{ _("The logs provided here aim to inform you of the most relevant server activity.") }}

## {{ _("Log files") }}

{{ _("LiberaForms saves the logs in files. The first file contains the most recent and the last, the oldest.") }}

{{ _("Eight files are kept for you to consult. Older logs are deleted and forgotten.") }}

## {{ _("Log levels") }}

{{ _("There are five different log levels.")}}

DEBUG -> INFO -> WARNING -> ERROR -> CRITICAL

{{ _("Only `INFO` and `WARNING` are displayed to you here.") }}

{{ _("System administrators may consult all log levels directly on the server.")}}

## {{ _("Log categories") }}

{% for filter in site.server_log_filters() %}`{{filter}}` {% endfor %}

{{ _("Select a category to filter the logs.") }}

## {{ _("Logs") }}

{{ _("Each log is made up of four parts.") }}

1. {{ _("The date and time") }}
2. {{ _("The log level") }}
3. {{ _("The category") }}
4. {{ _("A message") }}

---

[{{ _("Consult the server logs") }}<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>]({{url_for('site_bp.server_logs')}})
