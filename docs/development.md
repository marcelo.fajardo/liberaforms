# Set up development environment

Follow the instructions in `docs/INSTALL.md`

Use a domain that resolves to the machine running Flask.

Edit `/etc/hosts` and add

`127.0.0.1 dev-liberaforms.localdomain`

## Nginx

Read `docs/nginx.example`

You will need to create a self signed cert

`sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/nginx-selfsigned.key -out /etc/ssl/certs/nginx-selfsigned.crt`

Modify your nginx conf like this:

```
server {
    listen 443 ssl;
    server_name dev-liberaforms.localdomain;
    ssl_certificate           /etc/ssl/certs/nginx-selfsigned.crt;
    ssl_certificate_key       /etc/ssl/private/nginx-selfsigned.key;
    ....
```

## `.env`

### BASE_URL

`BASE_URL="https://dev-liberaforms.localdomain"`

### Flask config

`FLASK_DEBUG=True`

`FLASK_CONFIG=development`

# SASS development

We use gulp to compile SASS. Do something like

```
sudo npm -i -g gulp-cli
```

Install node requirements
```
cd liberaforms/static/sass
npm install
```

Watch/complie sass
```
cd liberaforms/static/sass
gulp
```
