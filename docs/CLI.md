# Command line utilities

> Command lines utilities required to install LiberaForms are mentioned in the `docs/INSTALL.md`

## Users

You can create a user when needed.

Note that the emails of users created via the command line do not require validation.

```
flask user create <username> <email> <password> -role=[guest|editor|admin]
```

Disable and enable users.

```
flask user disable <username>
flask user enable <username>
```

Set user properties

```
flask user set <username> -password= -email=
```

### Set new user defaults

See a list of valid language codes in `assets/language_codes.json`
```
flask user set-default -language=en
```

## SMTP

Configure SMTP

```
-host: Your SMTP server's name (required)
-port: SMTP port (required)
-user: SMTP username
-password: SMTP password
-encryption: [ SSL | STARTTLS ]
-noreply: Emails will be sent using this address (required)
```

```
flask smtp set -host=localhost -port=25 -noreply=no-reply@domain.com
```

Note: If your password contains the special characters you will need to wrap your password in single quotes.
```
-password='my!password'
```

View SMTP config
```
flask smtp get
```

Test SMTP config
```
flask smtp test -recipient=me@my.domain.com
```

## Disk usage

Returns the total size of uploaded files.

Optionally send an alert to admin emails if `TOTAL_UPLOADS_LIMIT` is exceeded (think cronjob).
```
flask storage usage --send-email-alert
```

## Forms

Expire forms with an old exipry date condition (run periodically with a cronjob)
```
flask site expire-forms
```

## Site

### Set custom languages

```
flask site set -language=en -language=ca
```

### Versions

Get the current Liberaforms and alembic database schema versions.

```
flask site get-versions
```
