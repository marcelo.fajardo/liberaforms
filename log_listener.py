"""
https://docs.python.org/3/howto/logging-cookbook.html#running-a-logging-socket-listener-in-production
https://gist.github.com/vsajip/4b227eeec43817465ca835ca66f75e2b

Copyright (C) 2020 Red Dove Consultants Limited. BSD-3-Clause licensed.
"""

import os
import traceback
import pickle
import socketserver
import select
import struct
import sys
import logging
import logging.config
from bisect import bisect
from typing import Dict
from logging import Formatter, LogRecord  # getLogger
from urllib.parse import urlparse
from dotenv import load_dotenv

# CRITICAL:50 <- ERROR:40 <- WARNING:30 <- INFO:20 <- DEBUG:10

env = os.path.join(os.getcwd(), '.env')
load_dotenv(dotenv_path=env)

PRINT_EXC_TYPE = False
LOG_SERVER_PORT = int(os.environ['LOG_SERVER_PORT']) if 'LOG_SERVER_PORT' in os.environ else 9000
LOG_LEVEL = os.environ['LOG_LEVEL'] if 'LOG_LEVEL' in os.environ else 'INFO'
SERVER_NAME = urlparse(os.environ['BASE_URL']).netloc
LOG_DIR = os.environ['LOG_DIR']
LOG_MAX_BYTES = 5_000_000
LOG_COPIES = 8

os.makedirs(LOG_DIR, exist_ok=True)

# DEFAULT_LOG_FORMAT = {
#     "datefmt": "%Y-%m-%d %H:%M:%S",
#     "style": "%"
# }

logging_conf = {
    "version": 1,
    "disable_existing_loggers": False,
    "handlers": {
        "file": {
            "class": "logging.handlers.RotatingFileHandler",
            "filename": f"{LOG_DIR}/{SERVER_NAME}.app.log",
            "maxBytes": LOG_MAX_BYTES,
            "backupCount": LOG_COPIES,
        }
    },
    "loggers": {
        "gunicorn.error": {
            "level": LOG_LEVEL,
            "handlers": ["file"],
            "propagate": False
        },
        "root": {
            "level": LOG_LEVEL,
            "handlers": ["file"]
        }
    }
}


class LevelFormatter(Formatter):
    # https://stackoverflow.com/a/68154386
    def __init__(self, formats: Dict[int, str], **kwargs):
        super().__init__()

        if 'fmt' in kwargs:
            raise ValueError(
                'Format string must be passed to level-surrogate formatters, '
                'not this one'
            )

        self.formats = sorted(
            (level, Formatter(fmt, **kwargs)) for level, fmt in formats.items()
        )

    def format(self, record: LogRecord) -> str:
        idx = bisect(self.formats, (record.levelno,), hi=len(self.formats)-1)
        level, formatter = self.formats[idx]
        return formatter.format(record)


class LogRecordStreamHandler(socketserver.StreamRequestHandler):
    """
    Handler for a streaming logging request.

    This basically logs the record using whatever logging policy is
    configured locally.
    """

    def handle(self):
        """
        Handle multiple requests - each expected to be a 4-byte length,
        followed by the LogRecord in pickle format. Logs the record
        according to whatever policy is configured locally.
        """
        while True:
            chunk = self.connection.recv(4)
            if len(chunk) < 4:
                break
            slen = struct.unpack('>L', chunk)[0]
            chunk = self.connection.recv(slen)
            while len(chunk) < slen:
                chunk = chunk + self.connection.recv(slen - len(chunk))
            obj = self.un_pickle(chunk)
            record = logging.makeLogRecord(obj)
            self.handle_log_record(record)

    def un_pickle(self, data):
        return pickle.loads(data)

    def handle_log_record(self, record):
        # if a name is specified, we use the named logger rather than the one
        # implied by the record.
        if self.server.logname is not None:
            name = self.server.logname
        else:
            name = record.name
        logger = logging.getLogger(name)
        # print("logger name:", name)

        # N.B. EVERY record gets logged. This is because Logger.handle
        # is normally called AFTER logger-level filtering. If you want
        # to do filtering, do it at the client end to save wasting
        # cycles and network bandwidth!
        logger.handle(record)


class LogRecordSocketReceiver(socketserver.ThreadingTCPServer):
    """
    Simple TCP socket-based logging receiver suitable for testing.
    """

    allow_reuse_address = True

    def __init__(self,
                 host='localhost',
                 port=logging.handlers.DEFAULT_TCP_LOGGING_PORT,
                 handler=LogRecordStreamHandler):
        socketserver.ThreadingTCPServer.__init__(self, (host, port), handler)
        self.abort = 0
        self.timeout = 1
        self.logname = None

    def serve_until_stopped(self):
        abort = 0
        while not abort:
            rd, wr, ex = select.select([self.socket.fileno()], [], [],
                                       self.timeout)
            if rd:
                self.handle_request()
            abort = self.abort


def main():
    logging.config.dictConfig(logging_conf)
    logging.getLogger().handlers[0].setFormatter(LevelFormatter({
        logging.DEBUG: "%(asctime)s %(levelname)s %(module)s:%(funcName)s %(message)s",
        logging.INFO: "%(asctime)s %(levelname)s %(message)s",
        logging.WARNING: "%(asctime)s %(levelname)s %(message)s",
        logging.ERROR: "%(asctime)s %(levelname)s %(module)s:%(funcName)s:%(lineno)s %(message)s",
        logging.CRITICAL: "%(asctime)s %(levelname)s %(module)s:%(funcName)s:%(lineno)s %(message)s"}
    ))
    tcpserver = LogRecordSocketReceiver(port=LOG_SERVER_PORT)
    print(f'About to start TCP log server on port {LOG_SERVER_PORT} ...')
    logging.getLogger('log_listener').info("APP - Started TCP log server")
    tcpserver.serve_until_stopped()


if __name__ == '__main__':
    try:
        rc = main()
    except KeyboardInterrupt:
        rc = 2
    except Exception as e:
        if PRINT_EXC_TYPE:
            s = ' %s:' % type(e).__name__
        else:
            s = ''
        sys.stderr.write('Failed:%s %s\n' % (s, e))
        if LOG_LEVEL == 'DEBUG':
            traceback.print_exc()
        rc = 1
    sys.exit(rc)
