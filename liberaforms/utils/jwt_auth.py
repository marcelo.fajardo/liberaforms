"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import jwt
from functools import wraps
from flask import request, current_app, jsonify
from liberaforms.models.formauth import FormAuth


def instantiate_form(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        token = None
        if "Authorization" in request.headers and \
           request.headers["Authorization"].startswith("Bearer "):
            token = request.headers["Authorization"][7:]
        if not token:
            return jsonify({'message': 'A valid token is missing'}), 401
        try:
            data = jwt.decode(token,
                              current_app.config['SECRET_KEY'],
                              algorithms=["HS256"])
            form_auth = FormAuth.find(token=data["token"])
            if not form_auth:
                return jsonify({'message': 'Not found'}), 404
            if not form_auth.enabled:
                return jsonify({'message': 'Not enabled'}), 410
            kwargs['form'] = form_auth.form
        except:
            return jsonify({'message': 'Not acceptable'}), 406
        return f(*args, **kwargs)
    return wrap
