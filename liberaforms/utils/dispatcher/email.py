"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import traceback
import smtplib
import ssl
from email.utils import formatdate, make_msgid
from flask import current_app
from flask_babel import gettext as _


class EmailServer():
    connection = None
    username = None
    password = None
    timeout = 7

    def __init__(self, site):
        self.host = site.smtp_config['host']
        self.port = site.smtp_config['port']
        self.use_ssl = True if site.smtp_config['encryption'] == 'SSL' else False
        self.use_tls = True if site.smtp_config['encryption'] == 'STARTTLS' else False
        self.username = site.smtp_config['user']
        self.password = site.smtp_config['password']
        self.default_sender = site.smtp_config['noreplyAddress']

    def open_connection(self):
        if self.use_ssl:
            try:
                self.connection = smtplib.SMTP_SSL( host=self.host,
                                                    port=self.port,
                                                    timeout=self.timeout)
                self.connection.login(self.username, self.password)
            except Exception as error:
                current_app.logger.warning("SMTP - Could not open connection")
                current_app.logger.error(traceback.format_exc())
                self.connection = None
                return error
        elif self.use_tls:
            try:
                self.connection = smtplib.SMTP( host=self.host,
                                                port=self.port,
                                                timeout=self.timeout)
                context = ssl.create_default_context()
                self.connection.ehlo()
                self.connection.starttls(context=context)
                self.connection.ehlo()
                self.connection.login(self.username, self.password)
            except Exception as error:
                current_app.logger.warning("SMTP - Could not open connection")
                current_app.logger.error(traceback.format_exc())
                self.connection = None
                return error
        else:
            try:
                self.connection = smtplib.SMTP( host=self.host,
                                                port=self.port,
                                                timeout=self.timeout)
                if self.username and self.password:
                    self.connection.login(self.username, self.password)
            except Exception as error:
                current_app.logger.warning("SMTP - Could not open connection")
                current_app.logger.warning("SMTP - Failed to send email")
                current_app.logger.error(traceback.format_exc())
                self.connection = None
                return error

    def close_connection(self):
        """Closes the connection with the email server."""
        if self.connection is None:
            return
        try:
            try:
                self.connection.quit()
            except (ssl.SSLError, smtplib.SMTPServerDisconnected):
                # This happens when calling quit() on a TLS connection
                # sometimes, or when the connection was already disconnected
                # by the connection.
                self.connection.close()
            except smtplib.SMTPException as e:
                current_app.logger.warning("SMTP - Could not close connection")
                current_app.logger.error(traceback.format_exc())
        finally:
            self.connection = None

    def send_mail(self, msg, emails):
        if 'SKIP_EMAILS' in os.environ and os.environ['SKIP_EMAILS'] == 'True':
            # TESTING environment sets 'SKIP_EMAILS'
            return {
                "email_sent": True
            }
        exception = self.open_connection()
        if exception:
            return {
                "email_sent": False,
                "msg": str(exception)
            }
        if self.connection:
            msg = self.prep_message(msg)
            try:
                self.connection.sendmail(msg['From'],
                                         emails,
                                         msg.as_string())
                self.close_connection()
                return {
                    "email_sent": True
                }
            except Exception as error:
                current_app.logger.warning("SMTP - Failed to send email")
                current_app.logger.error(traceback.format_exc())
                return {
                    "email_sent": False,
                    "msg": str(error)
                }
        current_app.logger.warning("SMTP - Failed to send email")
        return {
            "email_sent": False,
            "msg": _("Cannot connect to %(server)s", server=self.host)
        }

    def send_mail_async(self, app, msg, emails):
        with app.app_context():
            return self.send_mail(msg, emails)

    def prep_message(self, msg):
        msg['From'] = self.default_sender
        msg['Date'] = formatdate(localtime=True)
        msg['Message-ID'] = make_msgid()
        """
        admins=User.find_all(role='admin')
        if admins:
            msg['Errors-To'] = admins[0].email
        """
        return msg
