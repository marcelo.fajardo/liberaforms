"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
from flask import current_app, g, url_for
from flask_babel import gettext as _
from threading import Thread

from email.header import Header
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

from liberaforms.models.site import Site
from liberaforms.models.user import User
from liberaforms.utils.dispatcher.email import EmailServer
from liberaforms.utils.dispatcher.fediverse import FediPublisher
from liberaforms.utils import sanitizers



class Dispatcher(EmailServer):
    def __init__(self):
        self.site = Site.find()
        EmailServer.__init__(self, self.site)

    def create_multipart_message(self, text_body, html_body):
        text_part = MIMEText(text_body, _subtype='plain', _charset='UTF-8')
        html_part = MIMEText(html_body, _subtype='html', _charset='UTF-8')
        message = MIMEMultipart("alternative")
        message.attach(text_part)
        message.attach(html_part)
        return message

    def send_test_email(self, recipient_email):
        body = _("Congratulations!")
        message = MIMEText(body, _subtype='plain', _charset='UTF-8')
        message['Subject'] = Header(_("MAIL test")).encode()
        message['To'] = recipient_email
        status = self.send_mail(message, [recipient_email])
        return status

    def send_invitation(self, invite):
        text_body = invite.message
        message = MIMEText(text_body, _subtype='plain', _charset='UTF-8')
        message['Subject'] = Header(_("%(site_name)s. User invitation",
                                      site_name=self.site.name)).encode()
        message['To'] = invite.email
        status = self.send_mail(message, [invite.email])
        return status

    def send_invitation_accepted(self, inviter, invited_user):
        body = _("Your invitation has been accepted by %(email)s.", email=invited_user.email)
        greeting = _("Hello %(username)s", username=inviter.username)
        regards = _('Regards.')
        body = f"{greeting},\n\n{body}\n\n{regards}"
        message = MIMEText(body, _subtype='plain', _charset='UTF-8')
        message['Subject'] = Header(_("%(site_name)s. New user notification",
                                      site_name=self.site.name)).encode()
        message['To'] = inviter.email
        thr = Thread(
            target=self.send_mail_async,
            args=[current_app._get_current_object(), message, [inviter.email]]
        )
        thr.start()

    def send_account_recovery(self, user):
        link = url_for('user_bp.recover_password', token=user.token['token'], _external=True)
        body = _("Please use this link to create a new password.")
        greeting = _("Hello %(username)s", username=user.username)
        regards = _('Regards.')
        body = f"{greeting},\n\n{body}\n\n{link}\n\n{regards}"
        message = MIMEText(body, _subtype='plain', _charset='UTF-8')
        message['Subject'] = Header(_("%(site_name)s. Recover password",
                                      site_name=self.site.name)).encode()
        message['To'] = user.email
        # thread smtp to Prevent Timing Side Channel User Enumeration
        thr = Thread(
            target=self.send_mail_async,
            args=[current_app._get_current_object(), message, [user.email]]
        )
        thr.start()

    def send_email_address_confirmation(self, user, email_to_validate):
        if not user.is_validated:
            subject = _("Welcome to %(site_name)s", site_name=self.site.name)
            body = _('Before you get started we need you to validate your email.')
            body = body + "\n\n" + _('Please click on the link below.')
        else:
            # i18n: Validation of the new email address
            subject = _("%(site_name)s. Email validation", site_name=self.site.name)
            body = _("Use this link to validate your new email.")
        link = url_for('user_bp.validate_email', token=user.token['token'], _external=True)
        greeting = _("Hello %(username)s", username=user.username)
        regards = _('Regards.')
        body = f"{greeting},\n\n{body}\n\n{link}\n\n{regards}"
        message = MIMEText(body, _subtype='plain', _charset='UTF-8')

        message['Subject'] = Header(subject).encode()
        message['To'] = email_to_validate
        status = self.send_mail(message, [email_to_validate])
        return status

    def send_new_user_notification(self, user):
        admins = User.find_all(role='admin',
                               notifyNewUser=True,
                               validated_email=True,
                               blocked=False)
        admin_emails = [admin.email for admin in admins]
        if not admin_emails:
            return
        user_url = url_for('admin_bp.inspect_user', user_id=user.id, _external=True)
        body = _("New user '%(username)s' created at %(site_name)s.",
                 username=user.username,
                 site_name=self.site.name)
        greeting = _('Hello')
        regards = _('Regards.')
        body = f"{greeting},\n\n{body}\n\n{user_url}\n\n{regards}"
        message = MIMEText(body, _subtype='plain', _charset='UTF-8')
        message['Subject'] = Header(_("%(site_name)s. New user notification",
                                      site_name=self.site.name)).encode()
        message['To'] = ', '.join(admin_emails)
        thr = Thread(
            target=self.send_mail_async,
            args=[current_app._get_current_object(), message, admin_emails]
        )
        thr.start()

    def send_new_form_notification(self, form):
        admins = User.find_all(role='admin',
                               notifyNewForm=True,
                               validated_email=True,
                               blocked=False)
        admin_emails = [admin.email for admin in admins]
        if not admin_emails:
            return
        form_url = url_for('admin_bp.inspect_form', form_id=form.id, _external=True)
        body = _("New form '%(form_name)s' created at %(site_name)s.",
                 form_name=form.slug,
                 site_name=self.site.name)
        greeting = _('Hello')
        regards = _('Regards.')
        body = f"{greeting},\n\n{body}\n\n{form_url}\n\n{regards}"
        message = MIMEText(body, _subtype='plain', _charset='UTF-8')
        message['Subject'] = Header(_("%(site_name)s. New form notification",
                                      site_name=self.site.name)).encode()
        message['To'] = ', '.join(admin_emails)
        thr = Thread(
            target=self.send_mail_async,
            args=[current_app._get_current_object(), message, admin_emails]
        )
        thr.start()

    def send_new_answer_notification(self, formuser_emails, form):
        answers_url = url_for('answers_bp.list',
                              form_id=form.id,
                              _external=True)
        body = _("Your form '%(form_name)s' at '%(site_name)s' has been answered.",
                 form_name=form.name,
                 site_name=self.site.name)
        greeting = _('Hello')
        regards = _('Regards.')
        body = f"{greeting},\n\n{body}\n\n{answers_url}\n\n{regards}"
        message = MIMEText(body, _subtype='plain', _charset='UTF-8')
        message['Subject'] = Header(_("%(site_name)s. New form answer",
                                      site_name=self.site.name)).encode()
        message['To'] = ', '.join(formuser_emails)
        thr = Thread(
            target=self.send_mail_async,
            args=[current_app._get_current_object(), message, formuser_emails]
        )
        thr.start()

    def send_edited_answer_notification(self, formuser_emails, form):
        form_logs_url = url_for("form_bp.list_log",
                                form_id=form.id,
                                _external=True)
        body = _("An answer for '%(form_name)s' has been edited.", form_name=form.name)
        greeting = _('Hello')
        regards = _('Regards.')
        body = f"{greeting},\n\n{body}\n\n{form_logs_url}\n\n{regards}"
        message = MIMEText(body, _subtype='plain', _charset='UTF-8')
        message['Subject'] = Header(_("%(site_name)s. Edited form answer",
                                      site_name=self.site.name)).encode()
        message['To'] = ', '.join(formuser_emails)
        thr = Thread(
            target=self.send_mail_async,
            args=[current_app._get_current_object(), message, formuser_emails]
        )
        thr.start()

    def send_answer_confirmation(self, anon_email, form, answer):
        html_body = form.get_after_submit_text_html()
        if form.anon_edition:
            edition_link = form.get_edit_answer_link(answer)
            html_body = f"{html_body}\n\n<p>{edition_link}</p>"
        text_body = sanitizers.remove_html_tags(html_body)
        message = self.create_multipart_message(text_body, html_body)
        message['Subject'] = Header(_("Confirmation message")).encode()
        message['To'] = anon_email
        thr = Thread(
            target=self.send_mail_async,
            args=[current_app._get_current_object(), message, [anon_email]]
        )
        thr.start()

    def send_expired_form_notification(self, formuser_emails, form):
        form_url = url_for('form_bp.form_details', form_id=form.id, _external=True)
        body = _("The form '%(form_name)s' has expired at %(site_name)s.",
                 form_name=form.name,
                 site_name=self.site.name)
        greeting = _('Hello')
        regards = _('Regards.')
        body = f"{greeting},\n\n{body}\n\n{form_url}\n\n{regards}"
        message = MIMEText(body, _subtype='plain', _charset='UTF-8')
        message['Subject'] = Header(_("%(site_name)s. A form has expired",
                                      site_name=self.site.name)).encode()
        message['To'] = ', '.join(formuser_emails)
        thr = Thread(
            target=self.send_mail_async,
            args=[current_app._get_current_object(), message, formuser_emails]
        )
        thr.start()

    def send_error(self, error):
        if not current_app.config["ALERT_MAILS"]:
            current_app.logger.info('Cannot dispatch error. No ALERT_MAILS')
            return
        message = MIMEText(error, _subtype='plain', _charset='UTF-8')
        message['Subject'] = Header(_("Error at %(site_name)s",
                                      site_name=self.site.name)).encode()
        message['To'] = ', '.join(current_app.config["ALERT_MAILS"])
        thr = Thread(
            target=self.send_mail_async,
            args=[current_app._get_current_object(),
                  message,
                  current_app.config["ALERT_MAILS"]]
        )
        thr.start()

    def send_message(self, emails, subject, body):
        body = sanitizers.remove_html_tags(body)
        message = MIMEText(body, _subtype='plain', _charset='UTF-8')
        subject = f"{self.site.name}. {subject}"
        message['Subject'] = Header(subject).encode()
        message['To'] = ', '.join(emails)
        status = self.send_mail(message, emails)
        return status

    def publish_form(self, text, img_src, fediverse=True):
        published, msg = FediPublisher().publish(text, img_src)
        return {"published": published, "msg": msg}
