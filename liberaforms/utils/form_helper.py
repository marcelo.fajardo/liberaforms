"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2020 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""


from flask import url_for
from flask_babel import gettext as _
from liberaforms.utils import sanitizers


def _get_unique_value(values: list, value: str) -> str:
    """Return a value not present in values."""

    def strip_cnt(value) -> str:
        pos = value.rfind("_")
        if pos > -1:
            value = value[:pos]  # remove _{cnt}
        return value

    value = strip_cnt(value)
    cnt = 1
    while True:
        if value not in values:
            return value
        value = f"{strip_cnt(value)}_{cnt}"
        cnt = cnt + 1


def repair_form_structure(structure: list, form=None) -> list:
    """Sanitize form fields after saving formBuilder
       Ensure multi option fields have unique option.values"""

    if form and form.answers.count():
        multi_options_field_saved_data = form.get_multichoice_field_options_with_saved_data()
    else:
        multi_options_field_saved_data = {}

    #def field_has_answers(field_name: str, field_option: str) -> bool:
    #    return field_name in multi_options_field_saved_data.keys()

    def field_option_has_answers(field_name: str, field_option: str) -> bool:
        return field_name in multi_options_field_saved_data.keys() and \
               field_option in multi_options_field_saved_data[field['name']]

    def get_option_value(field: dict, option_label: str) -> str:

        option_value: str = option_label.replace(" ", "-")
        option_value = sanitizers.sanitize_string(option_value)
        option_value = sanitizers.sanitize_label(option_value)
        if len(option_value) > 43:
            option_value = f"{option_value[:40]}..."  # make value length 43 chars

        try:
            values = form.get_field(field["name"])["values"]
            existing_values = [option["value"] for option in values if option["value"]]
        except:
            existing_values = []
        submitted_values = [option["value"] for option in field["values"] if option["value"]]
        existing_values = list(set(existing_values + submitted_values))
        option_value = _get_unique_value(existing_values, option_value)

        return option_value

    repaired_structure = []
    for field in structure:
        if "type" in field:

            if field['type'] == 'paragraph':
                if "label" in field and field["label"]:
                    field["label"] = sanitizers.bleach_text(field["label"])
                if not field["label"]:
                    field["label"] = _('Paragraph')
                repaired_structure.append(field)
                continue

            if "label" in field and field["label"]:
                field['label'] = sanitizers.sanitize_label(field["label"])
            if not ("label" in field and field["label"]):
                # i18n: Refers to the name of a formfield
                field["label"] = _("Add an appropriate field name here")

            if "description" in field and field["description"]:
                field["description"] = sanitizers.sanitize_label(field["description"])

            # formBuilder does not save select dropdown correctly
            if field["type"] == "select" and "multiple" in field:
                if field["multiple"] is False:
                    del field["multiple"]  # <- this is the fix

            # multi option fields
            if field["type"] == "checkbox-group" or \
               field["type"] == "radio-group" or \
               field["type"] == "select":
                updated_options = []

                for option in field["values"]:  # iterate this field's options
                    option["value"] = option["value"].strip()

                    if option["label"]:
                        # sanitize the label
                        option["label"] = sanitizers.sanitize_label(option["label"])
                    if not option["label"] and option["value"]:
                        # editor submitted an existing option with a blank label
                        option["label"] = sanitizers.sanitize_label(option["value"])
                    if "selected" in option and field["type"] == "radio-group":
                        # unselect radio options
                        option["selected"] = False

                    # we need to ensure the option has a unique value
                    # 1. formBuilder does not enforce values for checkbox groups, radio groups and selects.
                    # 2. editor may have changed the label.

                    # if not field_has_answers(field["name"], option["value"]):
                    if not field_option_has_answers(field["name"], option["value"]):
                        # get a unique value based on the label
                        option["value"] = get_option_value(field, option["label"])

                    if option["label"] and option["value"]:
                        updated_options.append(option)
                field["values"] = updated_options
        repaired_structure.append(field)
    return repaired_structure


def status_badges(form, viewer_role) -> str:
    disabled = _("Disabled")
    expired = _("Expired")
    edit_mode =_("Edit mode")
    statement_required = _("Privacy statement required")
    if viewer_role == "editor":
        badges = ""
        if form.expired:
            badges = badges + f' <a href="{url_for("form_bp.expiration", form_id=form.id)}" class="badge rounded-pill text-bg-warning">{expired}</a>'
        if form.edit_mode:
            badges = badges + f' <a href="#" data-bs-toggle="modal" data-bs-target="#edit-mode-modal-{form.id}"><span class="badge rounded-pill text-bg-warning">{edit_mode}</span></a>'
        if form.requires_consent and not form.consents.count():
            badges = badges + f' <a href="{url_for("form_bp.inspect_form", form_id=form.id, goto="privacy-statements")}" class="badge rounded-pill text-bg-warning">{statement_required}</a>'
        if not form.admin_preferences['public']:
            badges = badges + f' <span class="badge rounded-pill bg-danger">{disabled}</span>'
        return badges
    if viewer_role == "admin":
        badges = ""
        if form.expired:
            badges = badges + f' <span class="badge rounded-pill bg-secondary">{expired}</span>'
        if form.edit_mode:
            badges = badges + f' <span class="badge rounded-pill bg-secondary">{edit_mode}</span>'
        if form.requires_consent and not form.consents.count():
            badges = badges + f' <span class="badge rounded-pill bg-secondary">{statement_required}</span>'
        if not form.admin_preferences['public']:
            badges = badges + f' <span class="badge rounded-pill bg-danger">{disabled}</span>'
        return badges
    return ""
