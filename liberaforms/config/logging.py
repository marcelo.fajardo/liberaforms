"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import logging
from logging.config import dictConfig


class LogSetup():

    use_log_server = True

    def __init__(self, app=None, **kwargs):
        if app is not None:
            self.init_app(app, **kwargs)

    def init_app(self, app):
        if app.config["FLASK_CONFIG"] == "development" or app.config["FLASK_CONFIG"] == "testing":
            self.use_log_server = False

        if self.use_log_server:
            os.makedirs(app.config["LOG_DIR"], exist_ok=True)
            log_config = {
                "version": 1,
                "disable_existing_loggers": False,
                "handlers": {
                  "socket": {
                    "class": "logging.handlers.SocketHandler",
                    "host": "localhost",
                    "port": app.config["LOG_SERVER_PORT"]
                  }
                },
                "loggers": {
                    "gunicorn": {
                        "level": app.config["LOG_LEVEL"],
                        "handlers": ["socket"],
                        "propagate": False
                    },
                    "root": {
                        "level": app.config["LOG_LEVEL"],
                        "handlers": ["socket"]
                    }
                }
            }
            logging.config.dictConfig(log_config)
        else:
            log_config = {
                "version": 1,
                "disable_existing_loggers": False,
                "root": {
                  "level": app.config["LOG_LEVEL"],
                },
            }
            logging.config.dictConfig(log_config)
