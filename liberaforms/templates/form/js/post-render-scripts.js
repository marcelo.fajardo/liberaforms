function postFormRender(){
  $("#liberaform").find("a").prop("target", "_blank")
  setLimits();
  {% if form.might_send_confirmation_email() %}
  watchEmail();
  {% endif %}
  {% if form.has_file_field() %}
    var allowed_extensions = "{{ ', '.join(g.site.mimetypes['extensions']).upper() }}";
    $("#liberaform").find('input[type=file]').each(function() {
      $(this).attr('data-parsley-max-file-size',
                   {{ config['MAX_ATTACHMENT_SIZE']/1024 }}
      );
      $(this).attr('data-parsley-filemimetypes',
                  "{{ ', '.join(g.site.mimetypes['mimetypes']) }}"
      );
      var hints = $('<div class="">')
      var hint = $('<span class="form-text">');
      hint.html('{{ _("Valid file types") }}'+': '+allowed_extensions)
      $(hints).append(hint)
      var hint = $('<span class="form-text">');
      hint.html('{{ _("The file should be no larger than %(size)s", size = human_readable_bytes(config["MAX_ATTACHMENT_SIZE"])) }}')
      $(hints).append(hint)
      $(this).after(hints)
    });
  {% endif %}
  $("#liberaform").show()
}

$(document).on("wheel", "input[type=number]", function (e) {
    $(this).blur();
});

function setLimits(){
  {% for field, values in form.expiry_conditions['fields'].items() %}
    {% if values["type"] == "number" %}
      var form_tally = {{ form.tally_number_field(field) }}
      {% if previous_answer %}
        if ($("#{{field}}").val() != "") {
          form_tally = form_tally - Number($("#{{field}}").val())
        }
      {% endif %}
      var available = {{ values["condition"] }} - form_tally
      if ($("#{{field}}").prop("max") && $("#{{field}}").prop("max") > available) {
        if (available > 0){
            var hint=$("<div class='form-text'>")
            var max_number_hint = ("{{ _('Maximum number is %(max)s', max=0) }}"); // 0 is a dummy value for i18n
            hint.text(max_number_hint.replace("0", available))
            hint.insertAfter("#{{field}}");
            $("#{{field}}").prop("max", available);
        }
      }
      if (! $("#{{field}}").prop("max")){
        $("#{{field}}").prop("max", available);
      }
    {% endif %}
  {% endfor %}
  return;
}

{% if form.might_send_confirmation_email() %}
function updateConfirmationMessage(email_address) {
  if (email_address) {
    var msg = "{{ _('Send me confirmation by email to {email_address}') }}"
    msg = msg.replace('{email_address}', email_address);
  }else{
    var msg = "{{ _('Send me confirmation by email') }}"
  }
  $("label[name='{{form.confirmation_field_name}}']").html(msg)
  if (isEmailValid(email_address)) {
    $("input[name='{{form.confirmation_field_name}}']").prop("disabled", false );
  }else{
    $("input[name='{{form.confirmation_field_name}}']").prop("disabled", true );
    $("input[name='{{form.confirmation_field_name}}']").prop("checked", false );
  }
}
function watchEmail() {
  $("input[type='email']").first().on('input change', function() {
    updateConfirmationMessage($(this).val())
  });
}
function isEmailValid(email) {
  return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)
  // TODO: this code might be better?
  /*
  is_valid = field.checkVadility()
  */
  //var re = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
  //var is_email=re.test(input.val());
}
{% endif %}
