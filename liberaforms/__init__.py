"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import logging
from datetime import datetime

from flask import Flask, request, redirect, flash, url_for
from flask_session import Session
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_babel import Babel
from flask_babel import gettext as _
from flask_wtf.csrf import CSRFProtect
from liberaforms.config.config import config
from liberaforms.config import logging as logging_config
from liberaforms.config.setup import ensure_directories


class CustomProxyFix():
    """Ensure URLs generated with url_for() use PREFERRED_URL_SCHEME."""

    def __init__(self, app, scheme):
        self.app = app
        self.scheme = scheme
    def __call__(self, environ, start_response):
        environ['wsgi.url_scheme'] = self.scheme
        return self.app(environ, start_response)


db = SQLAlchemy()
ma = Marshmallow()
babel = Babel()
session = Session()
csrf = CSRFProtect()
logs = logging_config.LogSetup()
login_manager = LoginManager()
login_manager.login_view = "user_bp.login"
login_manager.session_protection = "strong"


def create_app():
    """Create the Flask app."""
    app = Flask(__name__.split(".")[0])
    config_name = os.getenv('FLASK_CONFIG') or 'default'
    app.config.from_object(config[config_name])

    logs.init_app(app)
    db.init_app(app)
    ma.init_app(app)
    babel.init_app(app)
    session.init_app(app)
    login_manager.init_app(app)
    csrf.init_app(app)
    app.wsgi_app = CustomProxyFix(app.wsgi_app, app.config['PREFERRED_URL_SCHEME'])
    if app.config["FLASK_CONFIG"] == "production":
        from werkzeug.middleware.proxy_fix import ProxyFix
        app.wsgi_app = ProxyFix(app.wsgi_app, x_for=1)
    ensure_directories(app)

    if app.config["ENABLE_PROMETHEUS_METRICS"]:
        from werkzeug.middleware.dispatcher import DispatcherMiddleware
        from liberaforms.metrics import initialize_metrics
        from prometheus_client import make_wsgi_app
        # Prometheus monitoring activation
        app.wsgi_app = DispatcherMiddleware(app.wsgi_app, {
            '/metrics': make_wsgi_app()
        })
        initialize_metrics(app)

    from liberaforms.commands import register_commands
    register_commands(app)
    register_blueprints(app)
    app.jinja_env.add_extension('jinja2.ext.loopcontrols')

    app.logger.info(f"APP - Created app: { app.config['FLASK_CONFIG']}, LOG_LEVEL: {app.config['LOG_LEVEL']}")

    from liberaforms.utils import utils
    @app.before_request
    def before_request():
        if request.path[0:8] == '/static/':
            app.logger.warning('Serving a static file. Check Nginx config.')
        else:
            utils.populate_flask_g()

    #@app.context_processor
    #def inject_go_back_url():
    #    if request.method == 'GET':
    #        return dict(referrer_goback_url=request.referrer)


    from liberaforms.models.user import User
    @login_manager.user_loader
    def load_user(user_id):
        return User.find(id=user_id)

    @login_manager.unauthorized_handler
    def handle_needs_login():
        flash(_("Please log in to view this page"), "info")
        return redirect(url_for('user_bp.login', next=request.path))

    #@app.after_request
    #def after_request(response):
    #    """ Logging after every request. """
    #    if app.config["FLASK_CONFIG"] == "production":
    #        app.logger.info(
    #            "%s %s %s %s",
    #            request.method, request.path, response.status, request.referrer
    #        )
    #    return response
    return app


def register_blueprints(app) -> None:
    from liberaforms import views

    app.register_blueprint(views.errors_bp)
    app.register_blueprint(views.main_bp)
    app.register_blueprint(views.user_bp)
    app.register_blueprint(views.media_bp)
    app.register_blueprint(views.form_bp)
    app.register_blueprint(views.form_style_bp)
    app.register_blueprint(views.public_form_bp)
    app.register_blueprint(views.invite_bp)
    app.register_blueprint(views.site_bp)
    app.register_blueprint(views.admin_bp)
    app.register_blueprint(views.answers_bp)
    app.register_blueprint(views.consent_bp)
    app.register_blueprint(views.data_display.data_display_bp)
    app.register_blueprint(views.data_display.answers_diff_bp)
    app.register_blueprint(views.api_bp)

    return None
