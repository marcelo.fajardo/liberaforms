

var inline_help_cache = {}
var inline_help_history = []
$(function () {
  //$("#main-content").append(inline_help_container)
  //$(inline_help_container).insertBefore("#main-content")
})
/*
function inline_help_update_menus(file_name) {
  // highlight menu item
  $('#inline_help').find('a').removeClass("active")
  let menu_item = $('#inline_help').find('a[file-name="'+file_name+'"]')
  if (menu_item.length) {
    menu_item.addClass("active")
  } else {
    let merged_menus = Object.assign({}, inline_help_menu_data["user"], inline_help_menu_data["admin"])
    for (let item_file_name in merged_menus) {
      let item = merged_menus[item_file_name]
      if (Object.keys(item.sub_pages).includes(file_name)) {
        $('#inline_help').find('a[file-name="'+item_file_name+'"]').addClass("active")
        break;
      }
    }
  }
  if (inline_help_history.length > 1) {
    $("#inline_help_back_button").attr("href", "#")
  } else {
    $("#inline_help_back_button").removeAttr("href")
  }
}
*/
function _inline_help_history_push(file_name) {
  if (!(inline_help_history.length > 0 &&
        inline_help_history[inline_help_history.length - 1] == file_name)) {
    inline_help_history.push(file_name)
  }
}
function _hide_inline_help() {
  $('#inline_help').slideToggle(100)
  $('.ds-show-help').toggleClass("hide")
  $('.ds-hide-help').toggleClass("show")
}
function _inline_help_toggle(file_name) {
  inline_help_reset()
  if ($('#inline_help').is(':visible')) {
    _hide_inline_help()
  } else {
    if (!file_name) {
      file_name = (typeof inline_help_file_name !== 'undefined') ? inline_help_file_name : 'introduction.j2.md'
    }
    inline_help_retieve_page(file_name).then(function(page_data) {
      $('#inline_help_page_title').html(page_data.title)
      $('#inline_help_page').html(page_data.html)
      //inline_help_update_menus(file_name)
      $('#inline_help').slideDown(100)
      $('.ds-show-help').toggleClass("hide")
      $('.ds-hide-help').toggleClass("show")
    });
  }
}
function inline_help_goto_page(file_name) {
  inline_help_retieve_page(file_name).then(function(page_data) {
    $('#inline_help_page_title').html(page_data.title)
    $('#inline_help_page').html(page_data.html)
    //inline_help_update_menus(file_name)
  });
}
function inline_help_go_back() {
  if (inline_help_history.length > 1) {
    file_name = inline_help_history[inline_help_history.length - 2]
    inline_help_history.pop()
    inline_help_goto_page(file_name)
  } else {
    $("#help_button").trigger("click")
  }
}
function inline_help_reset() {
  inline_help_cache = {}
  inline_help_history = []
  $("input[type='search']").val("")
}
function inline_help_retieve_page(file_name) {
  return new Promise(function(resolve, reject) {
    if (Object.keys(inline_help_cache).includes(file_name)) {
      _inline_help_history_push(file_name)
      resolve(inline_help_cache[file_name])
      return
    }
    var data = {'file_name': file_name}
    if (Object.keys(inline_help_cache).length == 0) {
      data["with_setup"] = true
    }
    inline_help_cache[file_name] = {"html": "", "title": "Page not found"}
    $.ajax({
      url : "/site/help-page",
      type: "POST",
      dataType: "json",
      data: data,
      beforeSend: function(xhr, settings) {
        if (!/^(GET|HEAD|OPTIONS|TRACE)$/i.test(settings.type)) {
          xhr.setRequestHeader("X-CSRFToken", csrftoken)
        }
      },
      success: function(data, textStatus, jqXHR)
      {
        if (data.html) {
          inline_help_cache[file_name] = data
        }
        if (data.main_menu !== undefined) {
          $("#inline_help_menu").html(data.main_menu)
        }
        _inline_help_history_push(file_name)
        resolve(inline_help_cache[file_name])
      }
    });
  })
}
function search_inline_help(event) {
  if (event.key == "Enter") {
    do_search().then(function(data) {
      delete inline_help_cache["search"];
      let search_item_pos = inline_help_history.indexOf("search");
      if (search_item_pos > -1) {
        inline_help_history.splice(search_item_pos, 1)
      }
      var html = ""
      for (let page_name of Object.keys(data.result)) {
        let block = "<h2>" + page_name + "</h2>"
        lines = ""
        for (let line of data.result[page_name]) {
          lines = lines + line + "<br />"
        }
        html = html + block + lines
      }
      _inline_help_history_push("search")
      let page_title = $("input[type='search']").attr('placeholder') + ": " + $("input[type=search]").val()
      inline_help_cache["search"] = {"html": html, "title": page_title }
      $('#inline_help_page').html(html)
      $('#inline_help_page_title').html(page_title)
    });
  }
}

function do_search() {
  return new Promise(function(resolve, reject) {
    $.ajax({
      url : "/site/search-help",
      type: "POST",
      contentType: "application/json",
      data : JSON.stringify({"search_string": $("input[type=search]").val()}),
      dataType: "json",
      beforeSend: function(xhr, settings) {
        if (!/^(GET|HEAD|OPTIONS|TRACE)$/i.test(settings.type)) {
          xhr.setRequestHeader("X-CSRFToken", csrftoken)
        }
      },
      success: function(data, textStatus, jqXHR) {
        resolve(data) // Resolve promise and goto then()
      },
      error: function(err) {
        reject(err) // Reject the promise and goto catch()
      }
    });
  });
}
const inline_help_template = `
<div id="inline_help" class="grid ds-inline-help pt-2" style="display:none; --lf-gap: 0;">
  <div class="navbar g-col-12 ds-inline-help-page-header">
    <div>
      <a id="inline_help_back_button" href="#" onclick="js:inline_help_go_back()"" class="align-middle"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-left" aria-hidden="true"><polyline points="15 18 9 12 15 6"></polyline></svg><span>%Go_back%</span></a>
    </div>

    <div class="d-flex">
      <button class="ds-button-navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarHelpContent" aria-controls="navbarHelpContent" aria-expanded="false" aria-label="Toggle navigation">
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-more-vertical"><circle cx="12" cy="12" r="1"></circle><circle cx="12" cy="5" r="1"></circle><circle cx="12" cy="19" r="1"></circle></svg>
      </button>
      <div class="ds-inline_help_close_button" onclick="_hide_inline_help(); return">
        <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x" aria-hidden="true"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
      </div>
    </div>
  </div>
  <div id="inline_help_menu" class="g-col-12 g-col-lg-2 ds-inline-help-menu"></div>
  <div class="g-col-12 g-col-lg-10 ds-inline-help-page">
    <h1 id="inline_help_page_title"></h1>
    <div id="inline_help_page"></div>
  </div>
</div>
`
