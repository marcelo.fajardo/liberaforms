"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2020 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import json
import mimetypes
import markdown
import markupsafe
from feedgen.feed import FeedGenerator
from sqlalchemy.orm.attributes import flag_modified
from flask import g, request, render_template, redirect, Response
from flask import Blueprint, current_app, url_for
from flask import session, flash, jsonify
from jinja2 import Template
from flask_babel import gettext as _
from flask_babel import force_locale
import flask_login

from liberaforms.models.site import Site
from liberaforms.models.invite import Invite
from liberaforms.models.user import User
from liberaforms.models.form import Form
from liberaforms.models.consent import Consent
from liberaforms.models.schemas.consent import ConsentSchema
from liberaforms.models.formconsent import FormConsent
from liberaforms.domain.user import UserDomain
from liberaforms.utils import utils
from liberaforms.utils import auth
from liberaforms.utils import sanitizers
from liberaforms.utils import tokens
from liberaforms.utils import html_parser
from liberaforms.utils.chart_data import get_site_statistics
from liberaforms.utils.dispatcher import Dispatcher
from liberaforms.utils import wtf
from liberaforms.utils import i18n
from liberaforms.utils.inline_help import InlineHelp

# from pprint import pprint

site_bp = Blueprint('site_bp', __name__,
                    template_folder='../templates/site')


@site_bp.route('/site/recover-password', methods=['GET', 'POST'])
def recover_password():
    if flask_login.current_user.is_authenticated:
        flask_login.logout_user()
    wtform = wtf.GetEmail()
    if wtform.validate_on_submit():
        user = User.find(email=wtform.email.data, blocked=False)
        if user:
            user.save_token()
            Dispatcher().send_account_recovery(user)
        elif wtform.email.data == current_app.config['ROOT_USER']:
            # auto invite root user
            invite = Invite(email=wtform.email.data,
                            message="New root user",
                            token=tokens.create_token(),
                            role='admin',
                            invited_by_id=None)
            invite.save()
            return redirect(url_for('user_bp.create_new_user',
                                    invite=invite.token['token']))
        flash(_("We may have sent you an email"), 'info')
        return redirect(url_for('main_bp.index'))
    return render_template('user/recover-password.html', wtform=wtform)


@site_bp.route('/site/new-user-form', methods=['GET'])
@auth.enabled_admin_required
def new_user_form_preview():
    consents = ConsentSchema(
                many=True,
                only=('id', 'label', 'text', 'field_name', 'checkbox_label', 'required')
                ).dump(g.site.get_registration_consents())
    consent_library = ConsentSchema(many=True).dump(g.site.consents)
    return render_template('user/new-user.html',
                           wtform=wtf.NewUser(),
                           consents=consents,
                           consent_library=consent_library,
                           preview_only=True)


@site_bp.route('/site/new-user-form/enable-consent', methods=['POST'])
@auth.enabled_admin_required__json
def new_user_form_enable_consent():
    if not ('consent_id' in request.form and 'enable' in request.form):
        return jsonify("Not Acceptable"), 406
    consent_id = json.loads(request.form['consent_id'])
    enable = utils.str2bool(request.form['enable'])
    consent = Consent.find(id=consent_id, site_id=g.site.id)
    if not consent:
        return jsonify("Not found"), 404
    if consent.id in g.site.registration_consent and not enable:
        g.site.registration_consent.remove(consent.id)
    elif consent.id not in g.site.registration_consent and enable:
        g.site.registration_consent.append(consent.id)
    g.site.save()
    consents = ConsentSchema(
                many=True,
                only=('id', 'label', 'text', 'field_name', 'checkbox_label', 'required')
                ).dump(g.site.get_registration_consents())
    return jsonify(enabled_consents=consents), 200


# ## Site settings

@site_bp.route('/site/look-and-feel', methods=['GET'])
@auth.enabled_admin_required
def look_and_feel():
    wtcolorform = wtf.ChangeTheme()
    wtcolorform.primary_color.data = g.site.theme["primary_color"]
    wtcolorform.font_color.data = g.site.theme['navbar']['font_color']
    return render_template('site/look-and-feel.html',
                           wtcolorform=wtcolorform,
                           human_readable_bytes=utils.human_readable_bytes,
                           site=g.site)


@site_bp.route('/site/change-name', methods=['POST'])
@auth.enabled_admin_required
def change_name():
    if request.method == 'POST' and 'sitename' in request.form:
        name = sanitizers.remove_html_tags(request.form['sitename'])
        if name:
            g.site.name = name
            g.site.save()
    return redirect(url_for('site_bp.look_and_feel'))


@site_bp.route('/site/change-icon', methods=['POST'])
@auth.enabled_admin_required
def change_icon():
    if not request.files['file']:
        flash(_("Required file is missing"), 'warning')
        return redirect(url_for('site_bp.look_and_feel'))
    file = request.files['file']
    if "image/" in file.content_type:
        try:
            g.site.change_favicon(file)
            flash(_("Logo changed OK. Refresh with <F5>"), 'success')
        except Exception as error:
            current_app.logger.error(error)
    else:
        flash(_("An image file is required"), 'warning')
    return redirect(url_for('site_bp.look_and_feel'))


@site_bp.route('/site/reset-icon', methods=['GET'])
@auth.enabled_admin_required
def reset_icon():
    if g.site.reset_favicon():
        flash(_("Logo reset OK. Refresh with <F5>"), 'success')
    return redirect(url_for('site_bp.look_and_feel'))


@site_bp.route('/site/change-theme', methods=['POST'])
@auth.enabled_admin_required
def change_theme():
    wtform = wtf.ChangeTheme()
    if wtform.validate_on_submit():
        g.site.theme["primary_color"] = wtform.primary_color.data
        g.site.theme['navbar']['font_color'] = wtform.font_color.data
        g.site.save()
        flash(_("Colors updated OK"), 'success')
    return redirect(url_for('site_bp.look_and_feel'))


@site_bp.route('/site/new-user-language', methods=['GET', 'POST'])
@auth.enabled_admin_required
def set_new_user_language():
    if request.method == 'POST':
        if 'language' in request.form and \
          request.form['language'] in current_app.config['LANGUAGES']:
            g.site.newuser_language = request.form['language']
            g.site.save()
            flash(_("Language updated OK"), 'success')
            return redirect(url_for('admin_bp.admin_panel'))
    return render_template('new-user-language.html',
                           current_language=g.site.newuser_language)


@site_bp.route('/site/mimetypes', methods=['GET', 'POST'])
@auth.enabled_admin_required
def edit_mimetypes():
    wtform = wtf.FileExtensions()
    if wtform.validate_on_submit():
        mimetypes.init()
        updated_mimetypes = {"extensions": [], "mimetypes": []}
        extensions = wtform.extensions.data.splitlines()
        for extension in extensions:
            if not extension:
                continue
            mime_type = mimetypes.types_map[f".{extension}"]
            if extension not in updated_mimetypes["extensions"]:
                updated_mimetypes["extensions"].append(extension)
                updated_mimetypes["mimetypes"].append(mime_type)
        g.site.mimetypes = updated_mimetypes
        g.site.save()
        flash(_("Enabled file extensions updated OK"), 'success')
    if request.method == 'GET':
        wtform.extensions.data = '\n'.join(g.site.mimetypes['extensions'])
    return render_template('edit-mimetypes.html', wtform=wtform)


@site_bp.route('/site/toggle-invitation-only', methods=['POST'])
@auth.enabled_admin_required__json
def toggle_invitation_only():
    return jsonify(invite=g.site.toggle_invitation_only())


@site_bp.route('/site/toggle-newuser-uploads-default', methods=['POST'])
@auth.enabled_admin_required__json
def toggle_newuser_uploads_default():
    return jsonify(uploads=g.site.toggle_newuser_uploads_default())

# ## i18n texts


@site_bp.route('/site/custom-translations', methods=['GET'])
@auth.enabled_admin_required
def custom_languages():
    lang_codes = os.path.join(current_app.config['ASSETS_DIR'], 'language_codes.json')
    with open(lang_codes, 'r', encoding="utf-8") as all_languages:
        languages = json.load(all_languages)
    return render_template('site/custom-translations.html',
                           custom_languages=i18n.get_custom_languages(),
                           all_languages=languages)


@site_bp.route('/site/add-custom-language', methods=['POST'])
@auth.enabled_admin_required__json
def add_custom_language():
    if not ("lang_code" in request.form and request.form["lang_code"]):
        return jsonify("Not acceptable"), 406
    lang_code = request.form["lang_code"]
    if lang_code in g.site.custom_languages:
        return jsonify("Not acceptable"), 406
    lang_codes = os.path.join(current_app.config['ASSETS_DIR'], 'language_codes.json')
    with open(lang_codes, 'r', encoding="utf-8") as all_languages:
        languages = json.load(all_languages)
    selected_lang = [d for d in languages if d['value'] == lang_code]
    if not selected_lang:
        return jsonify("Not acceptable"), 406
    g.site.custom_languages.append(lang_code)
    g.site.save()
    enabled = [d for d in languages if d['value'] in g.site.custom_languages]
    return jsonify(custom_languages=i18n.sort_languages(enabled)), 200


@site_bp.route('/site/remove-custom-language', methods=['POST'])
@auth.enabled_admin_required__json
def remove_custom_language():
    if not ("lang_code" in request.form and request.form["lang_code"]):
        return jsonify("Not acceptable"), 406
    lang_code = request.form["lang_code"]
    if lang_code not in g.site.custom_languages:
        return jsonify("Not acceptable"), 406
    if lang_code == g.site.language:
        return jsonify("Not acceptable"), 406
    lang_codes = os.path.join(current_app.config['ASSETS_DIR'], 'language_codes.json')
    g.site.custom_languages.remove(lang_code)
    g.site.save()
    with open(lang_codes, 'r', encoding="utf-8") as all_languages:
        languages = json.load(all_languages)
    enabled = [d for d in languages if d['value'] in g.site.custom_languages]
    return jsonify(custom_languages=i18n.sort_languages(enabled)), 200


@site_bp.route('/site/default-custom-language', methods=['POST'])
@auth.enabled_admin_required__json
def set_custom_language():
    if not ("lang_code" in request.form and request.form["lang_code"]):
        return jsonify("Not acceptable"), 406
    lang_code = request.form["lang_code"]
    if lang_code not in g.site.custom_languages:
        return jsonify("Not acceptable"), 406
    lang_codes = os.path.join(current_app.config['ASSETS_DIR'], 'language_codes.json')
    with open(lang_codes, 'r', encoding="utf-8") as all_languages:
        languages = json.load(all_languages)
    if not [d for d in languages if d['value'] == lang_code]:
        return jsonify("Not acceptable"), 406
    g.site.save_default_language(lang_code)
    enabled = [d for d in languages if d['value'] in g.site.custom_languages]
    return jsonify(custom_languages=i18n.sort_languages(enabled),
                   default_language=g.site.language), 200


@site_bp.route('/site/front-page-text', methods=['GET', 'POST'])
@auth.enabled_admin_required
def edit_blurb():
    wtform = wtf.SiteBlurb()
    wtform.language.choices = i18n.get_language_select_choices()
    if wtform.validate_on_submit():
        g.site.save_blurb(wtform.blurb.data, wtform.language.data)
        flash(_("Text saved OK"), 'success')
    if request.method == 'GET':
        wtform.language.data = g.site.language
        wtform.blurb.data = g.site.blurb['front_page'][g.site.language]['markdown']
    return render_template('edit-blurb.html',
                           wtform=wtform,
                           upload_media_form=wtf.UploadMedia(),
                           human_readable_bytes=utils.human_readable_bytes)


@site_bp.route('/site/front-page-text/preview', methods=['POST'])
@auth.enabled_admin_required__json
def get_blurb_preview():
    if not ("blurb" in request.form and request.form["blurb"]):
        return jsonify(html=None)
    md_blurb = sanitizers.remove_html_tags(request.form["blurb"])
    return jsonify(html=sanitizers.markdown_to_html(md_blurb))


@site_bp.route('/site/resources-menu', methods=['GET', 'POST'])
@auth.enabled_admin_required
def edit_resources_menu():
    wtform = wtf.ResourcesMenu()
    wtform.language.choices = i18n.get_language_select_choices()
    if wtform.validate_on_submit():
        try:
            menu = json.loads(wtform.menu.data)
            if isinstance(menu, list):
                g.site.resources_menu["languages"][wtform.language.data] = menu
                flag_modified(g.site, "resources_menu")
                if len(g.site.custom_languages) > 1:
                    language = i18n.iso_code_2_language(wtform.language.data)
                    flash(_("Saved %(language)s menu OK", language=language), 'success')
                else:
                    flash(_("Saved menu OK"), 'success')
                g.site.save()
        except Exception as error:
            current_app.logger.debug(error)
    if request.method == 'GET':
        wtform.language.data = g.site.language
    template = Template(render_template('partials/resources-menu.html'))
    return render_template('resources-menu.html',
                           wtform=wtform,
                           new_nav_item=template.render())


@site_bp.route('/site/resources-menu/toggle', methods=['POST'])
@auth.enabled_admin_required__json
def toggle_resources_menu():
    g.site.resources_menu['enabled'] = not g.site.resources_menu['enabled']
    g.site.save()
    return jsonify(enabled=g.site.resources_menu['enabled'])


@site_bp.route('/site/invitation-template', methods=['GET', 'POST'])
@auth.enabled_admin_required
def invitation_template():
    wtform = wtf.InvitationText()
    wtform.language.choices = i18n.get_language_select_choices()
    if wtform.validate_on_submit():
        texts = g.site.invitation_text.copy()
        texts[wtform.language.data] = wtform.text.data
        g.site.invitation_text = texts
        g.site.save()
        flash(_("Saved template OK"), 'success')
    texts = g.site.invitation_text
    if wtform.errors:
        if "text" in wtform.errors:
            texts[wtform.language.data] = wtform.text.data

    if request.method == 'GET':
        wtform.language.data = g.site.language
    return render_template('invitation-template.html',
                           wtform=wtform,
                           texts=texts,
                           placeholders=Invite.placeholders())


@site_bp.route('/site/new-form-message', methods=['GET', 'POST'])
@auth.enabled_admin_required
def new_form_msg():
    wtform = wtf.NewFormMessage()
    wtform.language.choices = i18n.get_language_select_choices()
    if wtform.validate_on_submit():
        messages = g.site.new_form_msg["languages"]
        messages[wtform.language.data] = wtform.msg.data
        g.site.new_form_msg["languages"] = messages
        g.site.save()
        flash(_("New form message changed OK"), 'success')
    if request.method == 'GET':
        wtform.language.data = g.site.language
    messages = {}
    previews = {}
    for lang_code in g.site.new_form_msg["languages"]:
        messages[lang_code] = g.site.new_form_msg["languages"][lang_code]
        previews[lang_code] = g.site.get_new_form_msg(lang_code)
    return render_template('new-form-msg.html',
                           wtform=wtform,
                           messages=messages,
                           previews=previews)


@site_bp.route('/site/new-form-message/preview', methods=['POST'])
@auth.enabled_admin_required__json
def get_new_form_msg_preview():
    if not ("message" in request.form and request.form["message"]):
        return jsonify("Not acceptable"), 406
    message = markupsafe.Markup(sanitizers.bleach_text(request.form["message"]))
    msg = render_template('common/flash-message.html',
                          category='info',
                          message=message)
    return jsonify(html=msg)


@site_bp.route('/site/new-form-message/toggle', methods=['POST'])
@auth.enabled_admin_required__json
def toggle_new_form_msg():
    g.site.new_form_msg['enabled'] = not g.site.new_form_msg['enabled']
    g.site.save()
    return jsonify(enabled=g.site.new_form_msg['enabled'])


@site_bp.route('/site/other-information', methods=['GET', 'POST'])
@auth.enabled_admin_required
def edit_contact_info():
    wtform = wtf.ContactInformation()
    wtform.language.choices = i18n.get_language_select_choices()
    if wtform.validate_on_submit():
        contact_info = wtform.contact_info.data
        g.site.contact_info["languages"][wtform.language.data] = contact_info
        flag_modified(g.site, "contact_info")
        g.site.save()
        flash(_("Other information changed OK"), 'success')
    if request.method == 'GET':
        wtform.language.data = g.site.language
    return render_template('other-info.html',
                           messages=g.site.contact_info["languages"],
                           wtform=wtform)


@site_bp.route('/site/other-user-information/preview', methods=['POST'])
@auth.enabled_admin_required__json
def preview_other_info():
    json_request = request.get_json(silent=True)
    text = ""
    if "info_text" in json_request.keys():
        text = json_request["info_text"]
        text = sanitizers.bleach_text(text)
    return jsonify(html=text.replace('\n', '<br />') if text else ""), 200


@site_bp.route('/site/stats', methods=['GET'])
@auth.enabled_admin_required
def stats():
    return render_template('stats.html',
                           site=g.site,
                           stats=get_site_statistics())


@site_bp.route('/user/stats/year', methods=['GET'])
@auth.enabled_admin_required__json
def get_usage_graph_data():
    try:
        offset = int(request.args.get("year-offset", 0))
        return jsonify(chart_data=get_site_statistics(offset_year=offset))
    except:
        current_app.logger.debug("APP - Failed to get site usage graph data")
        return jsonify("Not acceptable"), 406


# ## SMTP config

@site_bp.route('/site/email/config', methods=['GET', 'POST'])
@auth.enabled_admin_required
def smtp_config():
    wtf_smtp = wtf.smtpConfig(**g.site.smtp_config)
    if wtf_smtp.validate_on_submit():
        if not wtf_smtp.encryption.data == "None":
            encryption = wtf_smtp.encryption.data
        else:
            encryption = ""
        config = {}
        config['host'] = wtf_smtp.host.data
        config['port'] = wtf_smtp.port.data
        config['encryption'] = encryption
        config['user'] = wtf_smtp.user.data
        config['password'] = wtf_smtp.password.data
        config['noreplyAddress'] = wtf_smtp.noreplyAddress.data
        g.site.save_smtp_config(**config)
        flash(_("Configuration saved OK"), 'success')
    wtf_email = wtf.GetEmail()
    return render_template('smtp-config.html',
                           wtf_smtp=wtf_smtp,
                           wtf_email=wtf_email)


@site_bp.route('/site/email/test-config', methods=['POST'])
@auth.enabled_admin_required
def test_smtp():
    wtform = wtf.GetEmail()
    if wtform.validate_on_submit():
        status = Dispatcher().send_test_email(wtform.email.data)
        if status['email_sent'] is True:
            if "smtp" in g.site.alerts:
                del g.site.alerts['smtp']
                g.site.save()
            current_app.logger.info("SMTP - Email server tests OK")
            flash(_("SMTP config works!"), 'success')
        else:
            flash(status['msg'], 'warning')
    else:
        flash("Email not valid", 'warning')
    return redirect(url_for('site_bp.smtp_config'))

# LDAP config


@site_bp.route('/site/ldap-config', methods=['GET'])
@auth.enabled_admin_required
def ldap_config():
    if not current_app.config['ENABLE_LDAP']:
        flash(_("LDAP config is not enabled"), 'error')
        return redirect(url_for('admin_bp.admin_panel'))
    return render_template('ldap-config.html')


@site_bp.route('/site/ldap/edit-filter', methods=['POST'])
@auth.enabled_admin_required__json
def ldap_edit_filter():
    if 'filter' in request.form and request.form['filter']:
        if '%uid' not in request.form['filter']:
            return jsonify(error="%uid is not declared in the filter")
        g.site.ldap_filter = request.form['filter']
        g.site.save()
    return jsonify(filter=g.site.get_ldap_filter())


@site_bp.route('/site/ldap/reset-filter', methods=['POST'])
@auth.enabled_admin_required__json
def ldap_reset_filter():
    g.site.ldap_filter = None
    g.site.save()
    return jsonify(filter=g.site.get_ldap_filter())


@site_bp.route('/site/ldap/test-bind', methods=['POST'])
@auth.enabled_admin_required__json
def ldap_test_bind():
    username = None
    if 'username' in request.form:
        username = sanitizers.sanitize_username(request.form['username'])
    password = request.form['password'] if 'password' in request.form else None
    domain = UserDomain(username, password)
    conn, result = domain.ldap_bind()
    domain.ldap_unbind(conn)
    return jsonify(result=result)


@site_bp.route('/site/ldap/test-search', methods=['POST'])
@auth.enabled_admin_required__json
def ldap_test_search():
    username = None
    if 'username' in request.form:
        username = sanitizers.sanitize_username(request.form['username'])
    password = request.form['password'] if 'password' in request.form else None
    domain = UserDomain(username, password)
    conn, msg = domain.ldap_bind()
    result, msg = domain.ldap_search(conn, request.form['param'])
    domain.ldap_unbind(conn)
    result.append(msg)
    return jsonify({'result': result})

# Wizard settings


@site_bp.route('/site/data-protection', methods=['GET'])
@auth.enabled_admin_required
def wizard_settings():
    orgprofile = wtf.OrganizationProfile()
    orgprofile.name.data = g.site.data_protection["organization"]['name']
    orgprofile.email.data = g.site.data_protection["organization"]['email']
    orgprofile.url.data = g.site.data_protection["organization"]['url']
    orgprofile.tos_url.data = g.site.data_protection["organization"]['tos_url']
    lawform = wtf.DataProtectionLaw()
    lawform.law.data = g.site.data_protection["law"]
    return render_template('site/data-protection-settings.html',
                           lawform=lawform,
                           orgprofile=orgprofile)


@site_bp.route('/site/data-protection/profile', methods=['POST'])
@auth.enabled_admin_required
def save_organization_profile():
    #if request.method == 'GET':
    #    return redirect(url_for('site_bp.wizard_settings'))
    wtform = wtf.OrganizationProfile()
    if wtform.validate_on_submit():
        if not g.site.data_protection["organization"]['name']:
            if "data_protection" in g.site.alerts:
                del g.site.alerts['data_protection']
                g.site.save()
        g.site.data_protection["organization"]['name'] = wtform.name.data
        g.site.data_protection["organization"]['email'] = wtform.email.data
        g.site.data_protection["organization"]['url'] = wtform.url.data
        g.site.data_protection["organization"]['tos_url'] = wtform.tos_url.data
        g.site.save_data_protection()
        flash(_("Saved organization profile OK"), 'success')
        return redirect(url_for('site_bp.wizard_settings'))
    lawform = wtf.DataProtectionLaw()
    lawform.law.data = g.site.data_protection["law"]
    return render_template('site/data-protection-settings.html',
                           orgprofile=wtform,
                           lawform=lawform)


@site_bp.route('/site/data-protection/law', methods=['GET', 'POST'])
@auth.enabled_admin_required
def save_data_protection_law():
    if request.method == 'GET':
        return redirect(url_for('site_bp.wizard_settings'))
    wtform = wtf.DataProtectionLaw()
    if wtform.validate_on_submit():
        g.site.data_protection["law"] = wtform.law.data
        g.site.save_data_protection()
        flash(_("Changed data protection law OK"), 'success')
        return redirect(url_for('site_bp.wizard_settings'))
    orgprofile = wtf.OrganizationProfile()
    orgprofile.name.data = g.site.data_protection["organization"]['name']
    orgprofile.email.data = g.site.data_protection["organization"]['email']
    orgprofile.url.data = g.site.data_protection["organization"]['url']
    return render_template('site/data-protection-settings.html',
                           lawform=wtform,
                           orgprofile=orgprofile)


@site_bp.route('/site/toggle-enforce-wizard-profile', methods=['POST'])
@auth.enabled_admin_required__json
def toggle_wizard_enforce_org():
    toggled = not g.site.data_protection["enforce_org"]
    g.site.data_protection["enforce_org"] = toggled
    g.site.save_data_protection()
    return jsonify({'enforced': toggled})


@site_bp.route('/site/toggle-dpl-is-public-administration', methods=['POST'])
@auth.enabled_admin_required__json
def toggle_wizard_public_administration():
    toggled = not g.site.data_protection["organization"]["is_public_administration"]
    g.site.data_protection["organization"]["is_public_administration"] = toggled
    g.site.save_data_protection()
    return jsonify({'public_administration': toggled})


@site_bp.route('/site/toggle-dpl-require', methods=['POST'])
@auth.enabled_admin_required__json
def toggle_wizard_require():
    toggled = not g.site.data_protection["require"]
    g.site.data_protection["require"] = toggled
    g.site.save_data_protection()
    return jsonify({'require': toggled}), 200

# Logs


@site_bp.route('/server/logs', methods=['GET'])
@site_bp.route('/server/logs/<int:log_number>', methods=['GET'])
@auth.enabled_admin_required
def server_logs(log_number=None):
    total_log_files = g.site.get_system_log_file_count()
    if log_number and log_number > total_log_files:
        log_number = None
    return render_template('site/server-logs.html',
                           total_log_files=total_log_files,
                           logs=g.site.get_server_logs(log_number),
                           active_log_number=log_number if log_number else 0)

# Inline help


@site_bp.route('/site/help-page', methods=['POST'])
@auth.authenticated_user_required__json
def get_inline_help():
    if 'file_name' not in request.form:
        error = "Page not found"
        current_app.logger.warning(error)
        return jsonify({'help_html': _(error)}), 200
    inline_help = InlineHelp(g.language, flask_login.current_user.role)
    payload = {
        'title': inline_help.get_page_label(request.form["file_name"]),
        'html': inline_help.get_page(request.form["file_name"])
    }
    if 'with_setup' in request.form:
        payload["main_menu"] = inline_help.get_menu()
    return jsonify(payload), 200


@site_bp.route('/site/search-help', methods=['POST'])
@auth.authenticated_user_required__json
def search_help():
    data = request.get_json()
    search_string = data["search_string"] if "search_string" in data else None
    if search_string:
        inline_help = InlineHelp(g.language, flask_login.current_user.role)
        result = inline_help.search(search_string)
        return jsonify(result=result), 200
    return jsonify(result={}), 200


# RSS

@site_bp.route('/feed', methods=['GET'])
@site_bp.route('/feed/<string:feed_type>', methods=['GET'])
def rss_feed(feed_type=None):
    """RSS feed of latest 10 published forms. Public information only."""
    if not current_app.config['ENABLE_RSS_FEED']:
        return redirect(url_for('main_bp.index'))
    feed_type = feed_type.lower() if feed_type else "rss"
    feed_type = feed_type if feed_type in ['rss', 'atom'] else "rss"
    fg = FeedGenerator()
    # i18n: feed_type can be, for example, RSS or Atom
    title = _("%(sitename)s %(feed_type)s feed", sitename=g.site.name, feed_type=feed_type)
    fg.id(f"{current_app.config['BASE_URL']}/feed/{feed_type}")
    fg.title(title)
    fg.link(href=current_app.config['BASE_URL'])
    blurb_html = g.site.blurb["front_page"][g.site.language]["html"]
    if feed_type == 'rss':
        fg.description(blurb_html)
    if feed_type == 'atom':
        fg.description(html_parser.extract_text(blurb_html, with_links=True))
    forms = Form.query.filter_by(enabled=True, restricted_access=False) \
                      .order_by(Form.created.desc()) \
                      .paginate(page=1, per_page=10) \
                      .items
    for form in forms:
        if not form.is_enabled():
            # disabled by admin
            continue
        feed_entry = fg.add_entry()
        feed_entry.id(form.url)
        feed_entry.pubDate(form.created)
        feed_entry.title(form.slug)
        feed_entry.link(href=form.url)
        if feed_type == 'rss':
            feed_entry.description(form.introduction_text['html'])
        if feed_type == 'atom':
            desc = html_parser.extract_text(form.introduction_text['html'],
                                            with_links=True)
            feed_entry.description(desc)
    if feed_type == 'rss':
        return Response(fg.rss_str(pretty=True), mimetype='application/rss+xml')
    return Response(fg.atom_str(pretty=True), mimetype='application/atom+xml')
