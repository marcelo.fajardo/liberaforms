"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2020 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
from functools import wraps
from flask import g, request, render_template, redirect
from flask import current_app, flash, url_for, session
from flask import Blueprint, send_file, after_this_request
from flask_babel import ngettext, gettext as _
import flask_login
from liberaforms.models.user import User
from liberaforms.models.form import Form
from liberaforms.models.formuser import FormUser
from liberaforms.models.answer import Answer, AnswerAttachment
from liberaforms.utils.dispatcher.dispatcher import Dispatcher
from liberaforms.utils import utils
from liberaforms.utils import auth
from liberaforms.utils import sanitizers
from liberaforms.utils import tokens
from liberaforms.utils.chart_data import answers_chart_data
from liberaforms.utils.exports import write_answers_csv

#from pprint import pprint as pp

answers_bp = Blueprint('answers_bp', __name__,
                       template_folder='../templates/answers')


def sanitized_key_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if not ('key' in kwargs and
                kwargs['key'] == sanitizers.sanitize_string(kwargs['key'])):
            return render_template('main/page-not-found.html'), 404
        return f(*args, **kwargs)
    return wrap


@answers_bp.route('/form/<int:form_id>/answers/<int:answer_id>/editions', methods=['GET'])
@answers_bp.route('/form/<int:form_id>/answers/<int:answer_id>/editions/<int:edition_id>', methods=['GET'])
@flask_login.login_required
@auth.enabled_editor_required
@auth.instantiate_form(allow_admin=False)
def answer_history(form_id, answer_id, edition_id=None, **kwargs):
    """Display an Answer's edition history."""

    form = kwargs["form"]
    answer = Answer.find(id=answer_id, form_id=form.id)

    if not (answer and answer.editions):
        flash(_("Cannot find that answer"), 'warning')
        return redirect(url_for('answers_bp.list', form_id=form.id))

    if not edition_id:
        edition_id = answer.id
    elif not answer.get_edition(edition_id):
        flash(_("Cannot find that edition"), 'warning')
        return redirect(url_for('answers_bp.list', form_id=form.id))

    def dump_edition(obj):
        if obj.__class__.__name__ == "Answer":
            created = utils.stringify_date_time(obj.updated)
            user_id = None
            is_most_recent = True
            is_selected = edition_id is None
            url = url_for('answers_bp.answer_history',
                          form_id=form.id, answer_id=answer.id)
        else:
            created = utils.stringify_date_time(obj.created)
            user_id = obj.user_id
            is_most_recent = False
            is_selected = obj.id == edition_id
            url = url_for('answers_bp.answer_history',
                          form_id=form.id, answer_id=answer.id, edition_id=obj.id)
        return {"id": obj.id,
                "created": created,
                "user_id": user_id,
                "is_most_recent": is_most_recent,
                "is_selected": is_selected,
                "url": url,
                "data": obj.data}

    editions = [dump_edition(answer)]
    for edition in answer.editions:
        editions.append(dump_edition(edition))

    if edition_id != answer.id:
        requested_edition = answer.get_edition(edition_id)
        if not requested_edition:
            flash(_("Cannot find that edition"), 'warning')
            return redirect(url_for('answers_bp.list', form_id=form.id))
        if requested_edition.previous_edition_id:
            compare_with_edition = answer.get_edition(requested_edition.previous_edition_id)
        else:
            compare_with_edition = requested_edition
        selected_edition = dump_edition(requested_edition)
        compare_with_edition = dump_edition(compare_with_edition)
    else:
        selected_edition = dump_edition(answer)
        compare_with_edition = dump_edition(answer.editions[0])

    if selected_edition["id"] != compare_with_edition["id"]:
        edition_author = User.find(id=compare_with_edition["user_id"])
    else:
        edition_author = None

    data_endpoint = url_for('answers_diff_bp.answers_diff',
                            form_id=form.id,
                            answer_id=answer.id,
                            edition_id=selected_edition["id"])

    return render_template('answers/history.html',
                           form=form,
                           answer=answer,
                           data_endpoint=data_endpoint,
                           editions=editions,
                           selected_edition_id=edition_id,
                           get_diff=utils.get_diff)


@answers_bp.route('/form/<int:form_id>/answers', methods=['GET'])
@flask_login.login_required
@auth.enabled_user_required
@auth.instantiate_form(allow_admin=False, allow_guest=True)
def list(form_id, display_as="", **kwargs):
    """Display the Answers to this Form."""
    form_user = kwargs["form_user"]
    form_user.save_ui_preference('landing_page', 'answers')
    display_as = form_user.ui_preferences["answers_landing_page"] if "answers_landing_page" in form_user.ui_preferences else ""
    return render_template('list-answers.html',
                           form=form_user.form,
                           display_as=display_as,
                           viewer_role="editor" if form_user.is_editor else "guest")


@answers_bp.route('/form/<int:form_id>/delete-all-answers', methods=['GET', 'POST'])
@auth.enabled_editor_required
@auth.instantiate_form(allow_admin=False)
def delete_all(form_id, **kwargs):
    form = kwargs['form']
    if request.method == 'POST':
        try:
            total_answers = int(request.form['totalAnswers'])
        except:
            flash(_("We expected a number"), 'warning')
            return render_template('delete-all-answers.html', form=form)
        if form.answers.count() == total_answers:
            form.delete_all_answers()
            username = flask_login.current_user.username
            current_app.logger.info(f'FORM - User "{username}" deleted all ({total_answers}) answers. form: {form.id}, "{form.slug}"')
            form.add_log(_("Deleted all answers"))
            if not form.has_expired() and form.expired:
                form.expired = False
                form.save()
            if form.author.set_disk_usage_alert():
                form.author.save()
            flash(ngettext("Deleted one answer",
                           "Deleted %(number)s answers",
                           total_answers,
                           number=total_answers), 'success')
            return redirect(url_for('answers_bp.list', form_id=form.id))
        flash(_("Number of answers does not match"), 'warning')
    return render_template('delete-all-answers.html', form=form)


@answers_bp.route('/form/<int:form_id>/attachment/<string:key>', methods=['GET'])
@flask_login.login_required
@auth.enabled_user_required
@auth.instantiate_form(allow_admin=False, allow_guest=True)
@sanitized_key_required
def download_attachment(form_id, key, **kwargs):
    form = kwargs["form"]
    attachment = AnswerAttachment.find(form_id=form_id, storage_name=key)
    if not attachment:
        current_app.logger.info(f'FORM - Cannot find requested answer attachment: {request.path}')
        return render_template('main/page-not-found.html'), 400
    (file_bytes, file_name) = attachment.get_attachment()
    try:
        return send_file(file_bytes,
                         download_name=file_name,
                         as_attachment=True)
    except Exception:
        current_app.logger.warning(f'FORM - Failed to download attachment. form: {form.id}, "{form.slug}", answer: {attachment.answer_id}')
        return render_template('main/page-not-found.html'), 404
