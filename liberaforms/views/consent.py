"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2022 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import json
from sqlalchemy import update
from flask import Blueprint, request, g, flash, jsonify
from flask import render_template, url_for, redirect
import flask_login
from flask_babel import gettext as _
from liberaforms import db
from liberaforms.models.consent import Consent
from liberaforms.models.form import Form
from liberaforms.models.formconsent import FormConsent
from liberaforms.models.schemas.consent import ConsentSchema
from liberaforms.models.schemas.form import FormSchema
from liberaforms.utils import auth
from liberaforms.utils import utils
from liberaforms.utils import sanitizers
from liberaforms.utils import wtf
from liberaforms.utils import i18n

# from pprint import pprint

consent_bp = Blueprint('consent_bp', __name__,
                       template_folder='../templates/consent')


@auth.enabled_editor_required
def handle_edit_consent_request(consent, scope=None):
    """Reusable code to create/edit consent texts in site, user and form views."""
    wtform = wtf.ConsentText()
    wtform.language_selector.choices = i18n.get_language_select_choices()
    if wtform.validate_on_submit():
        kwargs = {
            "name": wtform.name.data,
            "label": wtform.label.data,
            "md_text": wtform.md_text.data,
            "checkbox_label": wtform.checkbox_label.data,
            "required": utils.str2bool(wtform.required.data),
        }
        wizard = json.loads(wtform.wizard.data) if wtform.wizard.data else None
        if wizard:
            kwargs["wizard"] = wizard
        consent.save_texts(wtform.language.data, **kwargs)
        flash(_("Privacy statement saved OK"), 'success')
    if request.method == 'GET':
        wtform.name.data = consent.name
        lang_code = g.site.language if scope == "site" else g.language
        wtform.language.data = lang_code
        label = consent.label[lang_code] if lang_code in consent.label.keys() else ""
        md_text = consent.text[lang_code]["markdown"] if lang_code in consent.text.keys() else ""
        checkbox = consent.checkbox_label[lang_code] if lang_code in consent.checkbox_label.keys() else ""
        wtform.label.data = label
        wtform.md_text.data = md_text
        wtform.checkbox_label.data = checkbox
    wtform.required.data = consent.required
    return wtform


@consent_bp.route('/data-consent/preview', methods=['POST'])
@auth.enabled_editor_required__json
def get_preview():
    if not ("lang_code" in request.form and "required" in request.form):
        return jsonify("Not acceptable"), 406
    lang_code = request.form["lang_code"]
    kwargs = {}
    kwargs["required"] = utils.str2bool(request.form["required"])
    if "label" in request.form:
        kwargs["label"] = {lang_code: request.form["label"]}
    if "title" in request.form:
        kwargs["title"] = {lang_code: request.form["title"]}
    if "checkbox_label" in request.form:
        kwargs["checkbox_label"] = {lang_code: request.form["checkbox_label"]}
    if "md_text" in request.form:
        markdown = sanitizers.remove_html_tags(request.form["md_text"])
        html = sanitizers.markdown_to_html(markdown)
        html = sanitizers.bleach_text(html)
        kwargs["text"] = {lang_code: {"markdown": markdown,
                                      "html": html}}
    else:
        kwargs["text"] = {lang_code: {"markdown": "", "html": ""}}
    consent = ConsentSchema(lang_code=lang_code).dump(Consent(**kwargs))
    html = render_template('consent/partials/consent.html', consent=consent)
    return jsonify(html=html), 200


@consent_bp.route('/data-consent/<string:scope>/wizard', methods=['GET'])
@consent_bp.route('/data-consent/<int:consent_id>/<string:scope>/wizard', methods=['GET'])
@consent_bp.route('/data-consent/<int:consent_id>/<string:scope>/<int:form_id>/wizard', methods=['GET'])
@auth.enabled_editor_required__json
def get_wizard_data(scope, consent_id=None, form_id=None):
    """Return data required by wizard."""
    settings = {'scope': scope}
    if scope == "site" and flask_login.current_user.is_admin():
        organization = g.site.data_protection["organization"]
        settings["enforce_org"] = False
    else:
        organization = flask_login.current_user.get_data_protection_org()
        settings["enforce_org"] = g.site.data_protection["enforce_org"]
    consent = None
    if consent_id and scope == "site" and flask_login.current_user.is_admin():
        consent = Consent.find(id=consent_id, site_id=g.site.id)
    user_id = flask_login.current_user.id
    if consent_id and scope == "user":
        consent = Consent.find(id=consent_id, user_id=user_id)
    if consent_id and scope == "form" and form_id:
        form_consent = FormConsent.find(consent_id=consent_id, form_id=form_id)
        if not (form_consent and form_consent.form.is_editor(user_id)):
            return jsonify("Not found"), 404
        consent = form_consent.consent
    wizard = consent.wizard if consent else {}
    return jsonify(settings=settings,
                   default_organization=organization,
                   wizard=wizard), 200


@consent_bp.route('/data-consent/user/organization', methods=['PATCH'])
@auth.enabled_editor_required__json
def set_user_default_organization():
    data = request.get_json()
    profile = {}
    if "is_public_administration" in data and isinstance(data["is_public_administration"], bool):
        profile["is_public_administration"] = data["is_public_administration"]
    if "name" in data:
        profile["name"] = sanitizers.remove_html_tags(data["name"])
    if "email" in data:
        profile["email"] = sanitizers.remove_html_tags(data["email"])
    if "url" in data:
        profile["url"] = sanitizers.remove_html_tags(data["url"])
    if len(profile.keys()) != 4:
        return jsonify("Not acceptable"), 406
    flask_login.current_user.data_protection = profile
    flask_login.current_user.save()
    return jsonify("Patched"), 200


@consent_bp.route('/data-consent/accept-wizard-disclaimer', methods=['POST'])
@auth.enabled_editor_required__json
def accept_wizard_disclaimer():
    preference = not flask_login.current_user.preferences["wizard_disclaimer"]
    flask_login.current_user.preferences["wizard_disclaimer"] = preference
    flask_login.current_user.save()
    return jsonify(is_disclaimer_accepted=preference), 200


@consent_bp.route('/data-consent/<string:scope>/<int:consent_id>/forms', methods=['GET'])
@auth.enabled_editor_required__json
def attached_forms(scope, consent_id):
    """ Return a list of forms that use this consent """
    def get_consent_forms(consent_id):
        forms = []
        for form in FormConsent.find_all(consent_id=consent_id):
            forms.append(form.form)
        return forms
    if scope == "site" and flask_login.current_user.is_admin():
        consent = Consent.find(id=consent_id, site_id=g.site.id)
        if not consent:
            return jsonify("Not found"), 404
        if consent.enforced:
            forms = Form.find_all()
        else:
            forms = get_consent_forms(consent.id)
        return jsonify(forms=FormSchema(url_as_admin=True, many=True).dump(forms[:60])), 200
    if scope == "user":
        consent = Consent.find(id=consent_id,
                               user_id=flask_login.current_user.id)
        if not consent:
            return jsonify("Not found"), 404
        forms = get_consent_forms(consent.id)
        return jsonify(forms=FormSchema(url_as_editor=True, many=True).dump(forms[:60])), 200
    return jsonify("Not acceptable"), 406


@consent_bp.route('/data-consent/<string:scope>/<int:consent_id>/copy/forms', methods=['GET'])
@auth.enabled_editor_required__json
def template_forms(scope, consent_id):
    """ Return a list of forms that use a copy this consent """
    forms = []
    def get_consent_forms(consent_id):
        for form in FormConsent.find_all(consent_id=consent_id):
            forms.append(form.form)
    if scope == "site" and flask_login.current_user.is_admin():
        consent = Consent.find(id=consent_id, site_id=g.site.id)
        if not consent:
            return jsonify("Not found"), 404
        for consent in Consent.find_all(template_id=consent_id):
            get_consent_forms(consent.id)
        return jsonify(forms=FormSchema(url_as_admin=True, many=True).dump(forms[:60])), 200
    if scope == "user":
        consent = Consent.find(id=consent_id,
                               user_id=flask_login.current_user.id)
        if not consent:
            return jsonify("Not found"), 404
        for consent in Consent.find_all(template_id=consent_id):
            get_consent_forms(consent.id)
        return jsonify(forms=FormSchema(url_as_editor=True, many=True).dump(forms[:60])), 200
    return jsonify("Not acceptable"), 406


# List consents


@consent_bp.route('/site/data-consent', methods=['GET'])
@auth.enabled_admin_required
def list_site_data_consents():
    consents = ConsentSchema(many=True).dump(g.site.consents)
    return render_template('site/list-consents.html',
                           consents=consents,
                           Form=Form,
                           FormConsent=FormConsent)


@consent_bp.route('/user/data-consent', methods=['GET'])
@auth.enabled_editor_required
def list_user_data_consents():
    consents = ConsentSchema(many=True).dump(flask_login.current_user.consents)
    return render_template('user/list-consents.html',
                           consents=consents,
                           FormConsent=FormConsent)


@consent_bp.route('/form/<int:form_id>/data-consent', methods=['GET'])
@auth.enabled_editor_required
@auth.instantiate_form(allow_admin=False)
def list_form_data_consents(form_id, **kwargs):
    form = kwargs['form']
    consents = flask_login.current_user.get_available_consents()
    available_consents = [(consent.id, consent.name) for consent in consents]
    enabled_consents = ConsentSchema(many=True).dump(form.get_consents())
    return render_template('form/list-consents.html',
                           form=form,
                           enabled_consents=enabled_consents,
                           available_consents=available_consents)

# ## Edit consent


@consent_bp.route('/site/data-consent/new', methods=['GET', 'POST'])
@consent_bp.route('/site/data-consent/<int:consent_id>/edit', methods=['GET', 'POST'])
@auth.enabled_admin_required
def edit_site_data_consent(consent_id=None):
    if not consent_id:
        consent = Consent(site_id=g.site.id)
    else:
        consent = Consent.find(id=consent_id, site_id=g.site.id)
        if not consent:
            flash(_("Cannot find that statement"), 'error')
            return redirect(url_for('consent_bp.list_site_data_consents'))
    wtform = handle_edit_consent_request(consent, scope="site")
    if consent.id:
        post_action = url_for('consent_bp.edit_site_data_consent', consent_id=consent.id)
    else:
        post_action = url_for('consent_bp.edit_site_data_consent')
    return render_template('site/edit-consent.html',
                           consent=consent,
                           Form=Form,
                           FormConsent=FormConsent,
                           post_action=post_action,
                           wizard_url=url_for('consent_bp.wizard_for_site', consent_id=consent.id),
                           wtform=wtform)


@consent_bp.route('/user/data-consent/new', methods=['GET', 'POST'])
@consent_bp.route('/user/data-consent/<int:consent_id>/edit', methods=['GET', 'POST'])
@auth.enabled_editor_required
def edit_user_data_consent(consent_id=None):
    if not consent_id:
        consent = Consent(user_id=flask_login.current_user.id)
    else:
        consent = Consent.find(id=consent_id, user_id=flask_login.current_user.id)
        if not consent:
            flash(_("Cannot find that statement"), 'error')
            return redirect(url_for('consent_bp.list_user_data_consents'))
    wtform = handle_edit_consent_request(consent)
    if consent.id:
        post_action = url_for('consent_bp.edit_user_data_consent', consent_id=consent.id)
    else:
        post_action = url_for('consent_bp.edit_user_data_consent')
    return render_template('user/edit-consent.html',
                           consent=consent,
                           FormConsent=FormConsent,
                           post_action=post_action,
                           wizard_url=url_for('consent_bp.wizard_for_user', consent_id=consent.id),
                           wtform=wtform)


@consent_bp.route('/form/<int:form_id>/data-consent/copy/<int:template_id>', methods=['GET', 'POST'])
@consent_bp.route('/form/<int:form_id>/data-consent/<int:consent_id>/edit', methods=['GET', 'POST'])
@auth.enabled_editor_required
@auth.instantiate_form(allow_admin=False)
def edit_form_data_consent(form_id, template_id=None, consent_id=None, **kwargs):
    form = kwargs['form']
    template = None
    if template_id:
        # TODO: Auth. Can the user use this template?
        template = Consent.find(id=template_id)
        if not template:
            flash(_("Cannot find that statement"), 'error')
            return redirect(url_for('consent_bp.list_form_data_consents',
                                    form_id=form.id))
        consent = Consent.copy(template, mark_as_copy=True)
        consent.form_id = form.id
        consent.template_id = template.id
    if consent_id:
        consent = Consent.find(id=consent_id, form_id=form.id)
        if not consent:
            flash(_("Cannot find that statement"), 'error')
            return redirect(url_for('consent_bp.list_form_data_consents',
                                    form_id=form.id))
    wtform = handle_edit_consent_request(consent)

    if request.method == 'POST' and not wtform.errors:
        form.add_log(_('Edited privacy statement "%(name)s"', name=consent.name))
    if consent.id and template:
        # Unlink the library consent and link form to new consent
        old_rel = FormConsent.find(form_id=form.id, consent_id=template.id)
        if old_rel:
            old_rel.delete()
        FormConsent(form_id=form.id, consent_id=consent.id).save()
    if consent.id:
        post_action = url_for('consent_bp.edit_form_data_consent',
                              form_id=form.id,
                              consent_id=consent.id)
    elif template:
        post_action = url_for('consent_bp.edit_form_data_consent',
                              form_id=form.id,
                              template_id=template.id)
    return render_template('form/edit-consent.html',
                           form=form,
                           consent=consent,
                           template=template,
                           FormConsent=FormConsent,
                           post_action=post_action,
                           wizard_url=url_for('consent_bp.wizard_for_form',
                                              form_id=form.id,
                                              consent_id=consent.id),
                           wtform=wtform)
# ## Wizard


@consent_bp.route('/site/data-consent/wizard', methods=['GET'])
@consent_bp.route('/site/data-consent/<int:consent_id>/wizard', methods=['GET'])
@auth.enabled_admin_required
def wizard_for_site(consent_id=None):
    consent = Consent.find(id=consent_id, site_id=g.site.id)
    consent = consent if consent else Consent()
    endpoint = url_for('consent_bp.get_wizard_data', scope='site', consent_id=consent.id)
    consent_url = url_for('consent_bp.edit_site_data_consent', consent_id=consent.id)
    disclaimer = Consent.wizard_disclaimer(g.site.get_terms_of_service_link())
    return render_template('consent/wizard.html',
                           scope="site",
                           endpoint=endpoint,
                           consent_id=consent.id,
                           consent_url=consent_url,
                           go_back_url=consent_url,
                           disclaimer=disclaimer)


@consent_bp.route('/user/data-consent/wizard', methods=['GET'])
@consent_bp.route('/user/data-consent/<int:consent_id>/wizard', methods=['GET'])
@auth.enabled_editor_required
def wizard_for_user(consent_id=None):
    consent = Consent.find(id=consent_id, user_id=flask_login.current_user.id)
    consent = consent if consent else Consent()
    data_endpoint = url_for('consent_bp.get_wizard_data',
                            scope='user',
                            consent_id=consent.id)
    profile_endpoint = url_for('consent_bp.set_user_default_organization')
    consent_url = url_for('consent_bp.edit_user_data_consent', consent_id=consent.id)
    disclaimer = Consent.wizard_disclaimer(g.site.get_terms_of_service_link())
    return render_template('consent/wizard.html',
                           endpoint=data_endpoint,
                           profile_endpoint=profile_endpoint,
                           consent_id=consent.id,
                           consent_url=consent_url,
                           go_back_url=consent_url,
                           disclaimer=disclaimer)


@consent_bp.route('/form/<int:form_id>/data-consent/wizard', methods=['GET'])
@consent_bp.route('/form/<int:form_id>/data-consent/<int:consent_id>/wizard', methods=['GET'])
@auth.enabled_editor_required
@auth.instantiate_form(allow_admin=False)
def wizard_for_form(form_id, consent_id=None, **kwargs):
    form = kwargs['form']
    consent = None
    if consent_id:
        form_consent = FormConsent.find(consent_id=consent_id, form_id=form.id)
        if not form_consent:
            flash(_("Cannot find that privacy statement"), 'error')
            return redirect(url_for("form_bp.inspect_form", form_id=form.id))
        consent = form_consent.consent
    if consent:
        data_endpoint = url_for('consent_bp.get_wizard_data',
                                scope='form',
                                consent_id=consent.id,
                                form_id=form.id)
        consent_url = url_for('consent_bp.edit_form_data_consent',
                              form_id=form.id,
                              consent_id=consent.id)
        go_back_url = consent_url
    else:
        consent = Consent()
        data_endpoint = url_for('consent_bp.get_wizard_data', scope='form')
        consent_url = url_for('consent_bp.new_form_data_consent', form_id=form.id)
        go_back_url = url_for('form_bp.inspect_form', form_id=form.id)
    disclaimer = Consent.wizard_disclaimer(g.site.get_terms_of_service_link())
    return render_template('consent/wizard.html',
                           endpoint=data_endpoint,
                           consent_id=consent.id,
                           consent_url=consent_url,
                           go_back_url=go_back_url,
                           disclaimer=disclaimer)

# ## Delete consent


@consent_bp.route('/site/data-consent/delete', methods=['POST'])
@auth.enabled_admin_required__json
def delete_site_data_consent():
    if 'consent_id' not in request.form:
        return jsonify("Not Acceptable"), 406
    consent = Consent.find(id=json.loads(request.form['consent_id']), site_id=g.site.id)
    if consent and not (FormConsent.find(consent_id=consent.id)
                        or consent.id in g.site.registration_consent
                        or consent.enforced):
        consent.delete()
        return jsonify(deleted=True), 200
    return jsonify("Not Acceptable"), 406


@consent_bp.route('/user/data-consent/delete', methods=['POST'])
@auth.enabled_editor_required__json
def delete_user_data_consent():
    if 'consent_id' not in request.form:
        return jsonify("Not Acceptable"), 406
    consent = Consent.find(id=json.loads(request.form['consent_id']),
                           user_id=flask_login.current_user.id)
    if consent and not FormConsent.find(consent_id=consent.id):
        consent.delete()
        return jsonify(deleted=True), 200
    return jsonify("Not Acceptable"), 406


@consent_bp.route('/form/<int:form_id>/data-consent/delete', methods=['POST'])
@auth.enabled_editor_required__json
@auth.instantiate_form__json(allow_admin=False)
def delete_form_data_consent(form_id, **kwargs):
    """Delete consent when form_id != None
       Detach consent when form_id == None."""
    form = kwargs['form']
    if 'consent_id' not in request.form:
        return jsonify("Not Acceptable"), 406
    consent_id = json.loads(request.form['consent_id'])
    consent = Consent.find(id=consent_id, form_id=form.id)
    if consent:
        form.add_log(_('Deleted privacy statement "%(name)s"', name=consent.name))
        consent.delete()
        return jsonify(deleted=True), 200
    form_consent = FormConsent.find(form_id=form.id, consent_id=consent_id)
    if form_consent:
        form.add_log(_('Removed privacy statement "%(name)s"', name=form_consent.consent.name))
        form_consent.delete()
        return jsonify(deleted=True), 200
    return jsonify("Not Found"), 404


# Site consent

@consent_bp.route('/site/data-consent/<int:consent_id>/toggle-shared', methods=['POST'])
@auth.enabled_admin_required__json
def toggle_share_consent(consent_id):
    consent = Consent.find(id=consent_id, site_id=g.site.id)
    if consent:
        consent.shared = not consent.shared
        consent.save()
        return jsonify(shared=consent.shared), 200
    return jsonify("Not Acceptable"), 406


@consent_bp.route('/site/data-consent/<int:consent_id>/toggle-enforced', methods=['POST'])
@auth.enabled_admin_required__json
def toggle_enforced_consent(consent_id):
    consent = Consent.find(id=consent_id, site_id=g.site.id)
    if consent:
        consent.enforced = not consent.enforced
        if not consent.enforced:
            # ## Fringe condition
            # 1. editor sets form.requires_consent = True and attaches consent_x
            # 2. admin sets site consent_y.enforced = True
            # 3. editor removes attached consent_x from the form
            # 4. admin sets condition consent_y.enforced = False
            # 5. the form has no consents but form.requires_consent == True
            # 6. form.is_public() returns False :(
            stmt = (update(Form).
                    where((Form.consents == None) & (Form.requires_consent == True)).
                    values({"requires_consent": False}).
                    execution_options(synchronize_session="fetch"))
            # Note: clause "Form.consents == None" == "~Form.consents.any()"
            db.session.execute(stmt)
        if consent.enforced:
            consent.shared = False
        consent.save()  # commits
        return jsonify(enforced=consent.enforced,
                       consent_usage=render_template('consent/partials/usage.html',
                                                     consent=consent,
                                                     Form=Form,
                                                     FormConsent=FormConsent)), 200
    return jsonify("Not Acceptable"), 406


# Form consent

@consent_bp.route('/form/<int:form_id>/data-consent/new', methods=['GET', 'POST'])
@auth.enabled_editor_required
@auth.instantiate_form(allow_admin=False)
def new_form_data_consent(form_id, **kwargs):
    form = kwargs['form']
    if request.method == 'POST' and "add-to-library" in request.form:
        consent = Consent(user_id=flask_login.current_user.id)
    else:
        consent = Consent(form_id=form.id)
    wtform = handle_edit_consent_request(consent)
    if consent.id:
        FormConsent(form_id=form.id, consent_id=consent.id).save()
        form.add_log(_('Created privacy statement "%(name)s"', name=consent.name))
        if not form.requires_consent:
            form.requires_consent = True
            form.save()
        if form.consents.count() == 1:
            return redirect(url_for('form_bp.inspect_form', form_id=form.id))
        return redirect(url_for('consent_bp.list_form_data_consents', form_id=form.id))
    return render_template(
                'form/edit-consent.html',
                form=form,
                consent=consent,
                FormConsent=FormConsent,
                post_action=url_for('consent_bp.new_form_data_consent', form_id=form.id),
                wizard_url=url_for('consent_bp.wizard_for_form', form_id=form.id),
                go_back_url=url_for('form_bp.inspect_form', form_id=form.id),
                wtform=wtform)


@consent_bp.route('/form/<int:form_id>/data-consent/add', methods=['POST'])
@auth.enabled_editor_required__json
@auth.instantiate_form__json(allow_admin=False)
def link_consent_to_form(form_id, **kwargs):
    """Link a library consent to this form."""
    form = kwargs['form']
    if 'consent_id' not in request.form:
        return jsonify("Not Acceptable"), 406
    consent = Consent.find(id=json.loads(request.form['consent_id']))
    if not consent or FormConsent.find(form_id=form.id, consent_id=consent.id):
        return jsonify("Not Acceptable"), 406
    if consent.site_id and not consent.shared:
        return jsonify("Not Acceptable"), 406
    new_form_consent = FormConsent(form_id=form.id, consent_id=consent.id)
    new_form_consent.save()
    form.add_log(_('Added privacy statement "%(name)s"', name=consent.name))
    if not form.requires_consent:
        form.requires_consent = True
        form.save()
    consent = ConsentSchema().dump(consent)
    html = render_template('consent/partials/consent.html', consent=consent)
    return jsonify(consent=consent, html=html), 200
