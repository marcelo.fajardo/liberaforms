"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2020 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
from flask import g, request, render_template, redirect
from flask import Blueprint, current_app, url_for
from flask import flash, jsonify
from flask_babel import gettext as _
from flask_babel import refresh as babel_refresh
import flask_login
from liberaforms.models.user import User
from liberaforms.models.consent import Consent
from liberaforms.models.schemas.consent import ConsentSchema
from liberaforms.domain.user import UserDomain
from liberaforms.utils.dispatcher import Dispatcher
from liberaforms.utils import utils
from liberaforms.utils import validators
from liberaforms.utils import password as password_utils
from liberaforms.utils import auth
from liberaforms.utils import tokens
from liberaforms.utils import wtf
from liberaforms.utils import chart_data

#from pprint import pprint

user_bp = Blueprint('user_bp',
                    __name__,
                    template_folder='../templates/user')


@user_bp.route('/user/new', methods=['GET', 'POST'])
@user_bp.route('/user/new/<string:invite>', methods=['GET', 'POST'])
@tokens.instantiate_invite
def create_new_user(invite=None):
    if g.site.invitation_only and not invite:
        return redirect(url_for('main_bp.index'))
    flask_login.logout_user()
    if invite and invite.has_ldap_entry():
        return redirect(url_for('user_bp.login', invite=invite.token['token']))
    wtform = wtf.NewUser()
    consents = g.site.get_registration_consents()
    if wtform.validate_on_submit() and Consent.validate(request.form, consents):
        validated_email = False
        send_admin_notification = True
        role = 'editor'  # default role
        if invite:
            role = invite.role
            if invite.email == wtform.email.data:
                validated_email = True
                if wtform.email.data == current_app.config['ROOT_USER']:
                    # SMTP has not been configured yet
                    send_admin_notification = False
        elif wtform.email.data == current_app.config['ROOT_USER']:
            flash(_("Invitation only"), 'error')
            wtform.email.data = ""
            return render_template('new-user.html',
                                   consents=ConsentSchema(many=True).dump(consents),
                                   wtform=wtform)
        try:
            new_user = User(
                username=wtform.username.data,
                email=wtform.email.data,
                password=wtform.password.data,
                role=role,
                invited_by_id=invite.invited_by_id if invite else None,
                validated_email=validated_email,
            )
            new_user.save()
        except Exception as error:
            current_app.logger.error(error)
            flash(_("Opps! An error ocurred when creating the user"), 'error')
            return render_template('new-user.html')
        if invite:
            if invite.granted_form:
                domain = UserDomain(new_user.username, wtform.password.data)
                domain.grant_form_answers(invite)
            invite.delete()
        if not new_user.validated_email:
            new_user.save_token(email=new_user.email)
            Dispatcher().send_email_address_confirmation(new_user, new_user.email)
        if send_admin_notification:
            Dispatcher().send_new_user_notification(new_user)
        current_app.logger.info(f'USER - New user "{new_user.username}" with role "{new_user.role}" created.')
        flask_login.login_user(new_user)
        current_app.logger.info(f'USER - "{new_user.username}" logged in')
        babel_refresh()
        flash(_("Welcome!"), 'success')
        return redirect(url_for('user_bp.user_settings',
                                username=flask_login.current_user.username))
    flask_login.logout_user()
    if not wtform.email.data and invite:
        wtform.email.data = invite.email
    return render_template('new-user.html',
                           consents=ConsentSchema(many=True).dump(consents),
                           wtform=wtform)


@user_bp.route('/user/validate-to-continue', methods=['GET', 'POST'])
@auth.unvalidated_user_required
def validate_to_continue():
    wtform = wtf.NewEmailAddress()
    if wtform.validate_on_submit():
        flask_login.current_user.save_token(email=wtform.email.data)
        status = Dispatcher().send_email_address_confirmation(flask_login.current_user,
                                                              wtform.email.data)
        if status['email_sent'] is True:
            flash(_("We have sent an email to %(email)s", email=wtform.email.data), 'info')
        else:
            flash(status['msg'], 'warning')
            current_app.logger.warning(status['msg'])
    if request.method == 'GET':
        wtform.email.data = flask_login.current_user.email
    delete_account_link = utils.build_link(_("Changed your mind? You may $$delete your account here$$"),
                                           url_for('user_bp.delete_account'),
                                           _class="link-danger")
    return render_template('unvalidated/validate-email.html',
                           wtform=wtform,
                           delete_account_link=delete_account_link)


@user_bp.route('/user/validate-email/<string:token>', methods=['GET'])
@tokens.sanitized_token
def validate_email(token):
    """Use to validate an email."""
    user = User.find(token=token)
    if not user:
        flash(_("We couldn't find that petition"), 'warning')
        if flask_login.current_user.is_authenticated:
            return redirect(url_for('user_bp.user_settings',
                            username=flask_login.current_user.username))
        return redirect(url_for('main_bp.index'))
    if tokens.has_token_expired(user.token):
        flash(_("Your petition has expired"), 'warning')
        user.delete_token()
        if flask_login.current_user.is_authenticated:
            return redirect(url_for('user_bp.user_settings',
                            username=flask_login.current_user.username))
        return redirect(url_for('main_bp.index'))
    if 'email' in user.token:
        user.email = user.token['email']
        is_new_user = not user.validated_email
        if is_new_user:
            current_app.logger.info(f'USER - New user "{user.username}" validated email {user.email}')
            flash(_("Welcome!"), 'success')
        else:
            flash(_("Your new email is valid"), 'success')
        user.validated_email = True
        user.save()
    user.delete_token()
    flask_login.login_user(user)
    return redirect(url_for('user_bp.user_settings', username=user.username))

# ## Personal user settings


@user_bp.route('/user', methods=['GET'])
@user_bp.route('/user/<string:username>', methods=['GET'])
@flask_login.login_required
@auth.enabled_user_required
def user_settings(username=None):
    """Render the User settings page."""
    if username and username != flask_login.current_user.username:
        return redirect(url_for('user_bp.user_settings',
                                username=flask_login.current_user.username))
    if flask_login.current_user.update_alerts():
        flask_login.current_user.save()
    return render_template('user-settings.html',
                           human_readable_bytes=utils.human_readable_bytes)


@user_bp.route('/user/change-language', methods=['GET', 'POST'])
@auth.enabled_user_required
def change_language():
    if request.method == 'POST':
        if 'language' in request.form and \
           request.form['language'] in current_app.config['LANGUAGES']:
            flask_login.current_user.preferences["language"] = request.form['language']
            flask_login.current_user.save()
            babel_refresh()
            return redirect(url_for('user_bp.user_settings',
                            username=flask_login.current_user.username))
    current_language = flask_login.current_user.preferences["language"]
    return render_template('change-language.html', current_language=current_language)


@user_bp.route('/user/statistics', methods=['GET'])
@auth.enabled_user_required
def statistics():
    return render_template('user/statistics.html',
                           stats=chart_data.get_user_statistics(flask_login.current_user))


@user_bp.route('/user/statistics/year', methods=['GET'])
@auth.enabled_user_required__json
def get_user_usage_graph_data():
    try:
        offset = int(request.args.get("year-offset", 0))
        return jsonify(chart_data=chart_data.get_user_statistics(flask_login.current_user,
                                                                 offset_year=offset))
    except:
        current_app.logger.debug("APP - Failed to get user usage graph data")
        return jsonify("Not acceptable"), 406


@user_bp.route('/user/change-email', methods=['GET', 'POST'])
@auth.enabled_user_required
def change_email():
    wtform = wtf.NewEmailAddress()
    if wtform.validate_on_submit():
        flask_login.current_user.save_token(email=wtform.email.data)
        status = Dispatcher().send_email_address_confirmation(flask_login.current_user,
                                                              wtform.email.data)
        if status['email_sent'] is True:
            flash(_("We have sent an email to %(email)s", email=wtform.email.data), 'info')
        else:
            # TODO: Tell the user that the email has not been sent
            pass
        return redirect(url_for('user_bp.user_settings',
                                username=flask_login.current_user.username))
    return render_template('change-email.html', wtform=wtform)


@user_bp.route('/user/recover-password/<string:token>', methods=['GET'])
@auth.anonymous_user_required
@tokens.sanitized_token
def recover_password(token=None):
    if token:
        user = User.find(token=token)
        if not user:
            flash(_("Couldn't find that token"), 'warning')
            return redirect(url_for('main_bp.index'))
        if tokens.has_token_expired(user.token):
            flash(_("Your petition has expired"), 'warning')
            user.delete_token()
            return redirect(url_for('main_bp.index'))
        if user.blocked:
            user.delete_token()
            return redirect(url_for('main_bp.index'))
        user.delete_token()
        user.validated_email = True
        user.save()
        flask_login.login_user(user)
        return redirect(url_for('user_bp.reset_password'))
    flash(_("Not a valid link"), 'warning')
    return redirect(url_for('main_bp.index'))


@user_bp.route('/user/reset-password', methods=['GET', 'POST'])
@auth.enabled_user_required
def reset_password():
    wtform = wtf.ResetPassword()
    if wtform.validate_on_submit():
        flask_login.current_user.password_hash = password_utils.hash_password(wtform.password.data)
        flask_login.current_user.save()
        flash(_("Password changed OK"), 'success')
        return redirect(url_for('user_bp.user_settings'))
    return render_template('reset-password.html', wtform=wtform)


@user_bp.route('/user/password-quality', methods=['POST'])
def check_password_quality():
    content = request.get_json(silent=True)
    if "password" in content.keys() and content["password"]:
        password = content["password"]
        strength = password_utils.get_strength(password)
        return jsonify(strength=strength)
    return jsonify(strength=1)


@user_bp.route('/user/timezone', methods=['GET', 'POST'])
@auth.enabled_user_required
def change_timezone():
    wtform = wtf.ChangeTimeZone()
    if wtform.validate_on_submit():
        flask_login.current_user.timezone = wtform.timezone.data
        flask_login.current_user.save()
        flash(_("Time zone changed OK"), 'success')
        return redirect(url_for('user_bp.user_settings',
                                username=flask_login.current_user.username))
    timezones = {}
    tz_path = os.path.join(current_app.config['ASSETS_DIR'], 'timezones.txt')
    with open(tz_path, 'r', encoding="utf-8") as available_timezones:
        lines = available_timezones.readlines()
        for line in lines:
            line = line.strip()
            timezones[line] = line
    return render_template('change-timezone.html',
                           timezones=timezones,
                           wtform=wtform)


@user_bp.route('/user/avatar', methods=['GET', 'POST'])
@auth.enabled_user_required
def set_avatar():
    if request.method == 'POST':
        if "avatar_base64" in request.form:
            avatar_base64 = request.form["avatar_base64"]
            base64 = avatar_base64.replace("data:image/png;base64,", "")
            if validators.is_valid_base64_image(base64):
                flask_login.current_user.save_avatar(base64)
                flash(_("Avatar saved OK. You may need to refresh the page."), 'success')
                return jsonify(message="ok"), 200
        return jsonify("Not Acceptable"), 200
    return render_template('avatar.html')


@user_bp.route('/user/fediverse', methods=['GET', 'POST'])
@auth.enabled_user_required
def fediverse_config():
    if not flask_login.current_user.can_configure_fediverse():
        return redirect(url_for('user_bp.user_settings',
                                username=flask_login.current_user.username))
    wtform = wtf.FediverseAuth()
    if wtform.validate_on_submit():
        data = {"title": wtform.title.data,
                "node_url": wtform.node_url.data,
                "access_token": wtform.access_token.data}
        flask_login.current_user.set_fedi_auth(data)
        if not flask_login.current_user.fedi_auth:
            current_app.logger.warning('Could not encrypt access token')
            flash(_("Could not encrypt your access token"), 'warning')
        else:
            flash(_("Connected to the Fediverse"), 'success')
        flask_login.current_user.save()
        return redirect(url_for('user_bp.user_settings',
                                username=flask_login.current_user.username))
    if request.method == 'GET' and flask_login.current_user.fedi_auth:
        fedi_auth = flask_login.current_user.get_fedi_auth()
        if fedi_auth:  # successfully decrypted
            wtform.title.data = flask_login.current_user.fedi_connection_title()
            wtform.node_url.data = fedi_auth['node_url']
            wtform.access_token.data = fedi_auth['access_token']
    return render_template('fediverse-config.html', wtform=wtform)


@user_bp.route('/user/fediverse/delete-auth', methods=['POST'])
@auth.enabled_user_required
def fediverse_delete():
    flask_login.current_user.fedi_auth = {}
    flask_login.current_user.save()
    flash(_("Fediverse configuration deleted OK"), 'success')
    return redirect(url_for('user_bp.user_settings',
                            username=flask_login.current_user.username))


@user_bp.route('/user/delete-account', methods=['GET', 'POST'])
@auth.authenticated_user_required
def delete_account():
    if flask_login.current_user.is_admin() and User.find_all(role='admin').count() == 1:
        flash(_("Cannot delete. You are the only Admin on this site"), 'warning')
        return redirect(url_for('user_bp.user_settings',
                                username=flask_login.current_user.username))
    wtform = wtf.DeleteAccount()
    if wtform.validate_on_submit():
        flask_login.current_user.delete()
        username = flask_login.current_user.username
        current_app.logger.info(f'USER - User deleted. Goodbye "{username}"')
        flask_login.logout_user()
        flash(_("Thank you for using LiberaForms"), 'success')
        return redirect(url_for('main_bp.index'))
    if not flask_login.current_user.is_validated:
        return render_template('/user/unvalidated/delete-account.html', wtform=wtform)
    return render_template('/user/delete-account.html', wtform=wtform)


@user_bp.route('/user/toggle-new-answer-notification', methods=['POST'])
@auth.enabled_user_required__json
def toggle_new_answer_notification_default():
    return jsonify(default=flask_login.current_user.toggle_new_answer_notification_default())


@user_bp.route('/user/toggle-collapsed-nav-menu', methods=['POST'])
@auth.enabled_user_required__json
def toggle_collapsed_nav_menu():
    """Toggle the nav menu."""
    collapsed = not flask_login.current_user.preferences['collapsed_nav_menu']
    flask_login.current_user.preferences['collapsed_nav_menu'] = collapsed
    flask_login.current_user.save()
    return jsonify({'collapsed': collapsed}), 200

# Login / Logout


@user_bp.route('/user/login', methods=['GET', 'POST'])
@user_bp.route('/user/login/<string:invite>', methods=['GET', 'POST'])
@tokens.instantiate_invite
def login(invite=None):
    next_url = request.args.get('next', None)
    wtform = wtf.Login()
    if flask_login.current_user.is_authenticated:
        flask_login.logout_user()
    if wtform.validate_on_submit():
        domain = UserDomain(wtform.username.data, wtform.password.data)
        user = domain.get_user()
        if not user and current_app.config['ENABLE_LDAP']:
            user = domain.create_user_from_ldap()
        if user:
            if domain.validate_user():
                if user.blocked:
                    current_app.logger.info(f'DENY {request.remote_addr} - Blocked user "{user.username}" denied login')
                    flash(_("Bad credentials"), 'warning')
                    return redirect(url_for('user_bp.login'))
                if invite and invite.email == user.email:
                    if invite.granted_form:
                        domain.grant_form_answers(invite)
                    invite.delete()
                flask_login.login_user(user)
                current_app.logger.info(f'USER - "{user.username}" logged in')
                if not user.is_validated:
                    return redirect(url_for('user_bp.validate_to_continue'))
                return redirect(next_url or url_for("form_bp.my_forms"))
        utils.fake_a_login()
        current_app.logger.info(f'DENY {request.remote_addr} - Failed login. Bad credentials')
        flash(_("Bad credentials"), 'warning')
    if invite:
        wtform.username.data = invite.email
    return render_template('login.html',
                           wtform=wtform,
                           next=next_url,
                           token=invite.token['token'] if invite else None)


@user_bp.route('/user/logout', methods=['GET'])
@auth.authenticated_user_required
def logout():
    flask_login.logout_user()
    return redirect(url_for('main_bp.index'))


# Alerts

@user_bp.route('/user/hide-edit-mode-alert', methods=['POST'])
@auth.enabled_user_required__json
def toggle_edit_mode_reminder():
    """Toggle the edit-mode reminder modal displayed when editing public Forms."""
    preference = not flask_login.current_user.preferences['show_edit_alert']
    flask_login.current_user.preferences['show_edit_alert'] = preference
    flask_login.current_user.save()
    return jsonify(default=preference)


@user_bp.route('/user/alert/disk-full-modal', methods=['GET'])
@auth.enabled_user_required__json
def disk_full_modal():
    html = render_template('user/alerts/disk-full-modal.html')
    return jsonify(modal_html=html), 200


@user_bp.route('/user/alert/edit-mode-modal/<int:form_id>', methods=['GET'])
@auth.enabled_editor_required__json
@auth.instantiate_form()
def edit_mode_modal(form_id, **kwargs):
    form = kwargs["form"]
    if not "start_time" in form.edit_mode:
        duration = "UNKNOWN"
    else:
        duration = utils.get_fuzzy_duration(form.edit_mode['start_time'])
    html = render_template('user/alerts/edit-mode-modal.html',
                           form=form,
                           duration=duration)
    return jsonify(modal_html=html), 200
