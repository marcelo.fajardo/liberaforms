"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import io
import shutil
import typing
from datetime import datetime, timezone
import pyqrcode
from flask import current_app, url_for, g
from flask_babel import gettext as _
import flask_login
from liberaforms import db
from sqlalchemy.dialects.postgresql import JSONB, ARRAY, TIMESTAMP
from sqlalchemy.ext.mutable import MutableDict, MutableList
from sqlalchemy.orm.attributes import flag_modified

from liberaforms.models.log import FormLog
from liberaforms.models.formuser import FormUser
from liberaforms.models.consent import Consent
from liberaforms.models.formconsent import FormConsent
from liberaforms.models.answer import Answer, AnswerAttachment

from liberaforms.utils.database import CRUD
from liberaforms.utils.storage.remote import RemoteStorage
from liberaforms.utils import sanitizers
from liberaforms.utils import validators
from liberaforms.utils import html_parser
from liberaforms.utils import utils

# from pprint import pprint

class Form(db.Model, CRUD):
    """Form model definition."""

    __tablename__ = "forms"
    id = db.Column(db.Integer, primary_key=True, index=True)
    created = db.Column(TIMESTAMP, nullable=False)
    name = db.Column(db.String, nullable=False)
    slug = db.Column(db.String, unique=True, nullable=False)
    author_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    structure = db.Column(MutableList.as_mutable(ARRAY(JSONB)), nullable=False)
    preview = db.Column(MutableDict.as_mutable(JSONB), default={}, nullable=False)
    fieldIndex = db.Column(MutableList.as_mutable(ARRAY(JSONB)), nullable=False)
    enabled = db.Column(db.Boolean, default=False)
    expired = db.Column(db.Boolean, default=False)
    confirmation = db.Column(MutableDict.as_mutable(JSONB), nullable=False)
    requires_consent = db.Column(db.Boolean, default=False, nullable=False)
    expiry_conditions = db.Column(JSONB, nullable=False)
    restricted_access = db.Column(db.Boolean, default=False)
    admin_preferences = db.Column(MutableDict.as_mutable(JSONB), nullable=False)
    introduction_text = db.Column(MutableDict.as_mutable(JSONB), nullable=False)
    after_submit_text = db.Column(JSONB, nullable=False)
    expired_text = db.Column(JSONB, nullable=False)
    thumbnail = db.Column(db.String, nullable=True)
    published_cnt = db.Column(db.Integer, default=0, nullable=False)
    style = db.Column(MutableDict.as_mutable(JSONB), nullable=False)
    edit_mode = db.Column(JSONB, default={}, nullable=False)
    anon_edition = db.Column(db.Boolean, default=False, nullable=False)
    author = db.relationship("User", back_populates="authored_forms")
    answers = db.relationship("Answer",
                              lazy='dynamic',
                              cascade="all, delete, delete-orphan")
    log = db.relationship("FormLog",
                          lazy='dynamic',
                          order_by="desc(FormLog.created)",
                          cascade="all, delete, delete-orphan")
    users = db.relationship("FormUser",
                            lazy='dynamic',
                            cascade="all, delete, delete-orphan")
    consents = db.relationship("FormConsent",
                               lazy='dynamic',
                               order_by="desc(FormConsent.id)",
                               cascade="all, delete, delete-orphan")
    endpoint_auth = db.relationship("FormAuth",
                                    uselist=False,
                                    cascade="all, delete, delete-orphan")

    def __init__(self, author, **kwargs):
        """Create a new Form object."""
        self.created = datetime.now(timezone.utc)
        self.author_id = author.id
        self.name = kwargs["name"] if "name" in kwargs else ""
        self.slug = kwargs["slug"] if "slug" in kwargs else ""
        self.structure = kwargs["structure"] if "structure" in kwargs else []
        self.fieldIndex = Form.create_field_index(self.structure)
        if "introduction_text" in kwargs:
            self.introduction_text = kwargs["introduction_text"]
        else:
            self.introduction_text = Form.default_introduction_text(self.name)
        self.after_submit_text = Form.default_after_submit_text()
        self.set_short_description()
        self.expired_text = Form.default_expired_text()
        self.confirmation = {
            "send_email": self.structure_has_email_field(self.structure)
        }
        self.admin_preferences = {"public": True}
        self.style = {
            "colors": {},
            "font": "",
            "header_img": "",
            "background_img": "",
            "background_color": "",
        }
        self.expiry_conditions = {
            "totalAnswers": 0,
            "expireDate": False,
            "fields": {}
        }


    def __str__(self) -> str:
        """Use for debugging."""
        return utils.print_obj_values(self)

    #def save(self):
    #    super().save()
    #    enforced_consents = Consent.find_all(site_id=1, enforced=True)
    #    for enforced in enforced_consents:
    #        if not FormConsent.find(form_id=self.id, consent_id=enforced.id):
    #            FormConsent(form_id=self.id, consent_id=enforced.id).save()

    @property
    def site(self):
        """Site object used to build URLs."""
        return g.site

    @classmethod
    def find(cls, **kwargs):
        """Return first Form filtered by kwargs."""
        return cls.find_all(**kwargs).first()

    @classmethod
    def find_all(cls, **kwargs) -> list:
        """Return all Forms filtered by kwargs."""
        return cls.query.filter_by(**kwargs)
        # filters = []
        # filters.append(cls.editors.has_key(str(kwargs['editor_id'])))
        # filters.append(cls.sharedAnswers.contains({'key': kwargs['key']}))
        # return cls.query.filter(*filters)

    @classmethod
    def count(cls) -> int:
        """Return total of all Forms."""
        return cls.query.count()

    @classmethod
    def is_slug_available(cls, slug):
        if slug in current_app.config['RESERVED_SLUGS'] or cls.find(slug=slug):
            return False
        return True

    @property
    def url(self) -> str:
        """Return this form's pubic URL."""
        return url_for('public_form_bp.view', slug=self.slug, _external=True)

    def get_editors(self):
        return FormUser.find_all(form_id=self.id, is_editor=True)

    def get_created_date(self) -> str:
        """Return the Form's creation date."""
        return utils.utc_to_g_timezone(self.created).strftime("%Y-%m-%d")

    def change_author(self, new_author) -> bool:
        """Change the Author of the Form."""
        if new_author.id == self.author_id:
            return False
        if not new_author.enabled:
            return False
        self.author_id = new_author.id
        self.save()
        return True

    @staticmethod
    def create_field_index(structure: dict) -> list:
        """Build a field_index."""
        index = []
        # Add these RESERVED fields to the index.
        # i18n: Column title on table, to sort whether the form entries are marked or not
        index.append({'label': _("Marked"), 'name': 'marked'})
        # i18n: Used for sort items by creation order, almost always as column title
        index.append({'label': _("Created"), 'name': 'created'})
        for element in structure:
            if 'name' in element:
                if 'label' not in element:
                    element['label'] = _("Label")
                index.append({'name': element['name'], 'label': element['label']})
        return index

    def update_field_index(self, new_index: typing.List[dict]) -> None:
        """Update field_index.

        Add key/value 'removed'=True to fields that contain data.
        """
        if self.get_total_answers() == 0:
            self.fieldIndex = new_index
        else:
            deleted_fields_with_data = []
            # If the editor has deleted fields we want to remove them
            # but we don't want to remove fields that already contain data in the DB.
            for field in self.fieldIndex:
                if not [i for i in new_index if i['name'] == field['name']]:
                    # This field was removed by the editor. Can we safely delete it?
                    can_delete = True
                    for answer in self.answers:
                        if field['name'] in answer.data and \
                           answer.data[field['name']]:
                            # This field contains data
                            can_delete = False
                            break
                    if can_delete:
                        # A pseudo delete.
                        # We drop the field (it's reference) from the index
                        # (the empty field remains as is in each answer in the db)
                        pass
                    else:
                        # We don't delete this field from the index because it contains data
                        field['removed'] = True
                        deleted_fields_with_data.append(field)
                        flag_modified(self, "fieldIndex")
            self.fieldIndex = new_index + deleted_fields_with_data

    def start_edit_mode(self):
        self.edit_mode = {
            'form_id': self.id,
            'label': self.name,
            'editor_id': flask_login.current_user.id,
            'editor_email': flask_login.current_user.email,
            'start_time': str(datetime.now(timezone.utc))
        }
        self.preview = {}  # Should be {}, just in case.
        self.save()

    def get_field_index_for_display(self, with_deleted_columns=False) -> list:
        """Return the field_index used to display data."""
        result = []
        for field in self.fieldIndex:
            if 'removed' in field and not with_deleted_columns:
                continue
            item = {'label': field['label'], 'name': field['name']}
            result.append(item)
        for consent in self.get_consents():
            # append consent fields
            result.append({"name": consent.field_name, "label": consent.name})
        if result[1]['name'] == 'created':  # pos 1 should always be 'created'
            result.insert(len(result), result.pop(1))
        return result

    def get_deleted_fields(self) -> list:
        """Return a list of fields that have been removed from the Form.

        'removed' indicates that an answer to the field exists in the database.
        """
        return [field for field in self.fieldIndex if 'removed' in field]

    @staticmethod
    def is_email_field(field) -> bool:
        """Compare a field to an email field."""
        if "type" in field and field["type"] == "text" and \
           "subtype" in field and field["subtype"] == "email":
            return True
        return False

    @classmethod
    def structure_has_email_field(cls, structure) -> bool:
        """Use to find an email field in a form structure."""
        for element in structure:
            if cls.is_email_field(element):
                return True
        return False

    def has_email_field(self) -> bool:
        """Find an email field in this Form."""
        if self.edit_mode and self.preview:
            structure = self.preview["structure"]
        else:
            structure = self.structure
        return Form.structure_has_email_field(structure)

    def has_field(self, field_name: str) -> bool:
        """Find a field by name in this Form."""
        if self.edit_mode and self.preview:
            structure = self.preview["structure"]
        else:
            structure = self.structure
        for field in structure:
            if "name" in field and field["name"] == field_name:
                return True
        return False

    def has_file_field(self) -> bool:
        """Find a file upload field in this Form."""
        if self.edit_mode and self.preview:
            structure = self.preview["structure"]
        else:
            structure = self.structure
        for field in structure:
            if "type" in field and field["type"] == "file":
                return True
        return False

    def get_disk_usage(self) -> int:
        """Total bytes of this Form's attachments."""
        return AnswerAttachment.calc_total_size(form_id=self.id)

    @property
    def confirmation_field_name(self) -> str:
        """Use for the name of the checkbox field to solicit confirmation."""
        return "send-confirmation"

    def might_send_confirmation_email(self) -> bool:
        """Form meets the conditions to send a confirmation email."""
        if self.confirmation["send_email"] and self.has_email_field():
            return True
        return False

    def get_confirmation_email_address(self, answer) -> str:
        """Return the email address submitted with the Form."""
        for element in self.structure:
            if Form.is_email_field(element):
                if element["name"] in answer.data and answer.data[element["name"]]:
                    return answer.data[element["name"]].strip()
        return ""

    def get_answers(self, oldest_first=False, **kwargs) -> list:
        kwargs['oldest_first'] = oldest_first
        kwargs['form_id'] = self.id
        return Answer.find_all(**kwargs)

    def get_answers_for_display(self, oldest_first=False) -> typing.List[typing.Dict]:
        """Use for chart display and CSV export."""
        answers = self.get_answers(oldest_first=oldest_first)
        result = []
        for answer in answers:
            result.append({
                        'id': answer.id,
                        'created': utils.stringify_date_time(answer.created),
                        'marked': answer.marked,
                        **answer.data})
        return result

    def get_total_answers(self) -> int:
        """Total Form Answers."""
        return self.answers.count()

    def get_last_answer_date(self) -> str:
        """Return the date of the last submitted Answer."""
        last_answer = Answer.find(form_id=self.id)
        if last_answer:
            return utils.stringify_date_time(last_answer.created)
        return ""

    def get_embed_url(self) -> str:
        """URL to embed this Form in an iframe."""
        return f"{current_app.config['BASE_URL']}/embed/{self.slug}"

    def get_qr(self, as_png:bool=False) -> io.BytesIO:
        """Return the form's url as a QR png byte stream or svg string."""
        url = pyqrcode.create(self.url.encode('utf-8'), encoding='utf-8')
        stream = io.BytesIO()
        if as_png:
            url.png(stream, scale=12)
            return io.BytesIO(stream.getvalue())
        url.svg(stream, scale=7)
        return stream.getvalue().decode('utf-8')

    def get_opengraph(self) -> dict:
        """Opengraph values to include in HTML doc meta."""
        image_src = self.thumbnail if self.thumbnail else g.site.get_logo_uri()
        opengraph = {
            "title": self.slug,
            "url": self.url,
            "image": image_src,
            "description": self.get_short_description(),
        }
        return opengraph

    def set_short_description(self) -> None:
        """Set this Form's short desciption."""
        text = html_parser.get_opengraph_text(self.introduction_text['html'])
        self.introduction_text['short_text'] = text

    def get_short_description(self) -> str:
        """Return this Form's short description."""
        if 'short_text' in self.introduction_text:
            return self.introduction_text['short_text']
        self.set_short_description()
        self.save()
        return self.introduction_text['short_text']

    def get_consents(self) -> list:
        consents = []
        for form_consent in self.consents:
            #consents.append(Consent.find(id=form_consent.consent_id))
            consents.append(form_consent.consent)
        enforced_consents = db.session.query(Consent) \
                                      .filter(Consent.site_id != None,
                                              Consent.enforced == True).all()
                                      #  .order_by(Consent.id.asc()) \  # doesn't work?
        for enforced in enforced_consents:
            if enforced not in consents:
                consents.append(enforced)
        consents.sort(key=lambda c: c.id, reverse=True)
        return consents

    @staticmethod
    def default_introduction_text(name=None) -> dict:
        """Introduction text for new Forms."""
        # i18n: Example title in template for new form
        title = name if name else _("Form title")
        # i18n: Example content for new form.
        content = _("* Describe your form.\n* Add relevant content, links, images, etc.")
        mark_down = f"# {title}\n\n{content}"
        return {"markdown": mark_down,
                "html": sanitizers.markdown_to_html(mark_down)}

    def save_introduction_text(self, markdown: str) -> None:
        markdown = markdown.strip()
        if markdown:
            self.introduction_text = {'markdown': sanitizers.remove_html_tags(markdown),
                                      'html': sanitizers.markdown_to_html(markdown)}
        else:
            self.introduction_text = {'html': "", 'markdown': ""}
        self.set_short_description()
        self.save()

    @staticmethod
    def default_expired_text() -> typing.Dict[str, str]:
        """Text to display by default when Forms have expired."""
        text = _("Sorry, this form has expired.")
        return {"markdown": f"# {text}", "html": f"<h1>{text}</h1>"}

    def get_expired_text_html(self) -> str:
        if self.expired_text['html']:
            return self.expired_text['html']
        return Form.default_expired_text()["html"]

    def get_expired_text_markdown(self) -> str:
        if self.expired_text['markdown']:
            return self.expired_text['markdown']
        return Form.default_expired_text()["markdown"]

    def save_expired_text(self, markdown:str) -> None:
        markdown = markdown.strip()
        if markdown:
            self.expired_text = {'markdown': sanitizers.remove_html_tags(markdown),
                                 'html': sanitizers.markdown_to_html(markdown)}
        else:
            self.expired_text = {'html': "", 'markdown': ""}
        self.save()

    @staticmethod
    def default_after_submit_text() -> dict:
        """Text to display by default after Forms have been submitted."""
        # i18n: Thanks text displayed when completing form as user
        text = _("Thank you!")
        return {"markdown": f"# {text}", "html": f"<h1>{text}</h1>"}

    def get_after_submit_text_html(self) -> str:
        if self.after_submit_text['html']:
            return self.after_submit_text['html']
        return Form.default_after_submit_text()['html']

    def get_after_submit_text_markdown(self) -> str:
        if self.after_submit_text['markdown']:
            return self.after_submit_text['markdown']
        return Form.default_after_submit_text()['markdown']

    def save_after_submit_text(self, markdown: str) -> None:
        markdown = markdown.strip()
        if markdown:
            self.after_submit_text = {'markdown': sanitizers.remove_html_tags(markdown),
                                      'html': sanitizers.markdown_to_html(markdown)}
        else:
            self.after_submit_text = {'html': "", 'markdown': ""}
        self.save()

    def get_edit_answer_link(self, answer=None) -> str:
        # i18n: Two '%$$%' are required. Content between '$$g' is converted to a link.
        text = _('You can go back and $$change your answers here$$')
        if answer:
            return utils.build_link(text, answer.get_anon_edition_url())
        return utils.build_link(text, "javascript:void(0);")  # used for preview

    def get_available_number_type_fields(self) -> dict:
        """Return this Form's number type fields."""
        result = {}
        for element in self.structure:
            if "type" in element and element["type"] == "number":
                if element["name"] in self.expiry_conditions['fields']:
                    element_name = self.expiry_conditions['fields'][element["name"]]
                    result[element["name"]] = element_name
                else:
                    result[element["name"]] = {"type": "number",
                                               "condition": None}
        return result

    def get_number_fields(self) -> list:
        """Return this Form's number fields."""
        result = []
        for element in self.structure:
            if "type" in element and element["type"] == "number":
                result.append(element)
        return result

    def get_file_fields(self) -> list:
        """Return this Form's file fields."""
        result = []
        for element in self.structure:
            if "type" in element and element["type"] == "file":
                result.append(element)
        return result

    def get_multichoice_fields(self) -> list:
        """Return this Form's multi-choice fields."""
        result = []
        for element in self.structure:
            if "type" in element:
                if element["type"] == "checkbox-group" or \
                   element["type"] == "radio-group" or \
                   element["type"] == "select":
                    result.append(element)
        return result

    def get_field(self, field_name) -> dict:
        for field in self.structure:
            if 'name' in field and field['name'] == field_name:
                return field
        return {}

    def get_field_label(self, field_name) -> str:
        """Return a field's label."""
        field = self.get_field(field_name)
        return field["label"] if field and "label" in field else ""

    def get_answer_label(self, field_name, answer_value) -> str:
        """Use to return a string to be displayed as the answer."""
        label = ""
        for element in self.structure:
            if 'name' in element and element['name'] == field_name:
                if element["type"] == "checkbox-group" or \
                   element["type"] == "radio-group" or \
                   element["type"] == "select":
                    option_labels = []
                    values_without_labels = []
                    for value in answer_value.split(', '):
                        value_label = next((l for l in element['values']
                                            if l['value'] == value), None)
                        if value_label:
                            option_labels.append(value_label['label'])
                        else:
                            values_without_labels.append(value)
                    option_labels.extend(values_without_labels)
                    label = ', '.join(option_labels)
                    break
        if not label:
            label = answer_value
        return label

    def save_expiry_date(self, date: str) -> None:
        """Save the exipry date of this Form."""
        self.expiry_conditions['expireDate'] = date
        self.expired = self.has_expired()
        flag_modified(self, "expiry_conditions")
        self.save()

    def save_expiry_total_answers(self, total_answers: str) -> int:
        """Save the maximum permited total Answers of this Form."""
        try:
            total = int(total_answers)
        except:
            total = 0
        total = 0 if total < 0 else total
        self.expiry_conditions['totalAnswers'] = total
        self.expired = self.has_expired()
        flag_modified(self, "expiry_conditions")
        self.save()
        return self.expiry_conditions['totalAnswers']

    def save_expiry_field_condition(self, field_name, condition) -> bool:
        """Save the expiry condition of a number field."""
        available_fields = self.get_available_number_type_fields()
        if field_name not in available_fields:
            return False
        if not condition:
            if field_name in self.expiry_conditions['fields']:
                del self.expiry_conditions['fields'][field_name]
                self.expired = self.has_expired()
                flag_modified(self, "expiry_conditions")
                self.save()
            return False
        field_type = available_fields[field_name]['type']
        if field_type == "number":
            try:
                condition_dict = {"type": field_type, "condition": int(condition)}
                self.expiry_conditions['fields'][field_name] = condition_dict
            except:
                condition = False
                if field_name in self.expiry_conditions['fields']:
                    del self.expiry_conditions['fields'][field_name]
            self.expired = self.has_expired()
            flag_modified(self, "expiry_conditions")
            self.save()
            return condition
        return False

    def update_expiry_conditions(self) -> None:
        """Update conditions after editing this Form."""
        saved_expiry_fields = list(self.expiry_conditions['fields'])
        available_expiry_fields = []
        for element in self.structure:
            if "name" in element:
                available_expiry_fields.append(element["name"])
        for field in saved_expiry_fields:
            if field not in available_expiry_fields:
                del self.expiry_conditions['fields'][field]
                flag_modified(self, "expiry_conditions")

    def delete(self) -> None:
        """Delete a Form from the database."""
        self.delete_all_answers()  # removes attachment dirs/buckets
        super().delete()


    def get_attachment_dir(self) -> str:
        """Return os path to this Form's attachment directory."""
        return os.path.join(current_app.config['UPLOADS_DIR'],
                            current_app.config['ATTACHMENT_DIR'],
                            str(self.id))

    def delete_all_answers(self) -> None:
        """Delete this Form's answers.

        Attachments are deleted via SQL cascading.
        File blobs are deleted here.
        """
        self.answers.delete()
        self.expired = self.has_expired()
        self.save()
        attachment_dir = self.get_attachment_dir()
        if os.path.isdir(attachment_dir):
            shutil.rmtree(attachment_dir, ignore_errors=True)
        else:
            current_app.logger.debug(f"Local attachment dir not found: {attachment_dir}")
        if current_app.config['ENABLE_REMOTE_STORAGE'] is True:
            RemoteStorage().remove_directory(f"attachments/{self.id}")

    # def is_author(self, user) -> bool:
    #     """Compare user with this Form's author."""
    #     return self.author_id == user.id

    def is_editor(self, user_id) -> bool:
        """Given a user_id check if is editor (rw form setting)."""
        return bool(FormUser.find(form_id=self.id, user_id=user_id, is_editor=True))

    def is_guest(self, user_id) -> bool:
        """Given a user_id check if is guest (rw answers)."""
        return bool(FormUser.find(form_id=self.id, user_id=user_id, is_editor=False))

    def can_expire(self) -> bool:
        """Return True when expiry_conditions are defined."""
        if self.expiry_conditions["totalAnswers"]:
            return True
        if self.expiry_conditions["expireDate"]:
            return True
        if self.expiry_conditions["fields"]:
            return True
        return False

    def has_expired(self) -> bool:
        """Check if expiry_conditions have been met."""
        if not self.can_expire():
            return False
        if self.expiry_conditions["totalAnswers"] and \
           self.answers.count() >= self.expiry_conditions["totalAnswers"]:
            return True
        if self.expiry_conditions["expireDate"] and not \
           validators.is_future_date(self.expiry_conditions["expireDate"]):
            return True
        for field_name, value in self.expiry_conditions['fields'].items():
            if value['type'] == 'number':
                total = self.tally_number_field(field_name)
                if total >= int(value['condition']):
                    return True
        return False

    def tally_number_field(self, field_name) -> int:
        """Return the sum of a number field's answers."""
        total = 0
        for answer in self.answers:
            try:
                total = total + int(answer.data[field_name])
            except:
                continue
        return total

    def toggle_admin_form_public(self) -> bool:
        """Save Admin's preference to disable this Form."""
        self.admin_preferences['public'] = not self.admin_preferences['public']
        self.save()
        return self.admin_preferences['public']

    def is_enabled(self) -> bool:
        """Is this Form publically available."""
        if not (self.author.enabled and self.admin_preferences['public']):
            return False
        return self.enabled

    def toggle_enabled(self) -> bool:
        """Toggle publically available."""
        if self.expired or self.edit_mode or self.admin_preferences['public'] is False:
            return False
        self.enabled = not self.enabled
        self.save()
        return self.enabled

    def can_be_published(self) -> bool:
        """Can this Form be published."""
        if self.author.is_active() \
           and self.admin_preferences['public'] \
           and not (self.expired or self.edit_mode) \
           and not (self.requires_consent and not self.get_consents()):
            return True
        return False

    def is_public(self) -> bool:
        """Form's URL is publically available."""
        return bool(self.enabled and self.can_be_published())

    def get_expected_field_names_on_submit(self, is_edition=False) -> list:
        """Use to check submitted form field names."""

        field_names = []
        for field in self.structure:
            if "name" in field:
                field_name = field["name"]
                if field_name.startswith("checkbox-group"):
                    # formbuilder adds [] to field name
                    field_name = f"{field_name}[]"
                field_names.append(field_name)
        for consent in self.get_consents():
            field_names.append(consent.field_name)
        if self.confirmation["send_email"]:
            # extra field added to request confirmation
            field_names.append(self.confirmation_field_name)
        if is_edition:
            file_fields = self.get_file_fields()
            for file_field in file_fields:
                field_names.append(f"dummy-{file_field['name']}")
        return field_names

    def get_multichoice_field_options_with_saved_data(self) -> typing.Dict:
        """Use when editing a form.

        We don't want the Editor to change the option values if an
        answer.data[key] with a value is already present in the database.
        """
        result: typing.Dict = {}
        if not self.answers:
            return result
        multi_choice_fields: typing.Dict = {}  # {field.name: [option.value, option.value]}
        for field in self.get_multichoice_fields():
            multi_choice_fields[field['name']] = []
            for value in field['values']:
                multi_choice_fields[field['name']].append(value['value'])
        for answer in self.answers:
            removed_fields_from_search = []
            for field in multi_choice_fields:
                if field in answer.data.keys():
                    for saved_value in answer.data[field].split(', '):
                        if saved_value in multi_choice_fields[field]:
                            if field not in result:
                                result[field] = []
                            result[field].append(saved_value)
                            multi_choice_fields[field].remove(saved_value)
                            if multi_choice_fields[field] == []:
                                # all option.values are present in database
                                removed_fields_from_search.append(field)
            for field_to_remove in removed_fields_from_search:
                del multi_choice_fields[field_to_remove]
                if not multi_choice_fields:  # no more fields to check
                    return result
        return result

    def add_log(self, message, actor=None) -> None:
        """Add activity log."""
        actor = actor if actor else flask_login.current_user.username
        FormLog(username=actor, form_id=self.id, message=message).save()
