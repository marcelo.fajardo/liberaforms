"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

from flask import current_app
from flask_babel import gettext as _
from liberaforms import ma
from liberaforms.models.user import User
from liberaforms.models.formuser import FormUser


class UserSchemaForAdminDataDisplay(ma.SQLAlchemySchema):
    class Meta:
        model = User

    id = ma.auto_field()
    created = ma.auto_field()
    username = ma.auto_field()
    email = ma.auto_field()
    enabled = ma.Method('get_is_enabled')
    role = ma.Method('get_role')
    total_forms = ma.Method('get_total_forms')
    disk_usage = ma.Method('get_disk_usage')
    avatar_src = ma.Method('get_avatar_src')

    def get_is_enabled(self, obj):
        return obj.enabled

    def get_total_forms(self, obj):
        return FormUser.find_all(user_id=obj.id).count()

    def get_role(self, obj):
        if obj.role == 'editor':
            return _("Editor")
        if obj.role == 'guest':
            return _("Guest")
        if obj.role == 'admin':
            return _("Admin")

    def get_disk_usage(self, obj):
        if current_app.config['ENABLE_UPLOADS']:
            return obj.total_uploads_usage()
        return 0

    def get_avatar_src(self, obj):
        return obj.get_avatar_src()
