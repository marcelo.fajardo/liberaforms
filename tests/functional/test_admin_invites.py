"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
from faker import Faker
import factory
from flask import current_app, url_for
from liberaforms.models.site import Site
from liberaforms.models.user import User
from liberaforms.models.invite import Invite
from tests.factories import UserFactory
from tests import VALID_PASSWORD, DEFAULT_ROLE, DEFAULT_LANGUAGE
from tests import user_creds
from tests.utils import login, logout
from tests import utils


class TestAdminInvites():
    """Create and delete an invitation. Then create again and test new user form"""

    @classmethod
    def setup_class(cls):
        site = Site.find()
        cls.default_language = site.language
        cls.invitee_role = 'guest'
        cls.properties = {}


    def test_requirements(self):
        assert current_app.config['ENABLE_UPLOADS'] is True
        assert Invite.find_all().count() == 0
        assert User.find(email=user_creds['invitee']['email']) is None

    def test_auth(self, client):
        """Test admin_bp.new_invite
                admin_bp.delete_invite."""
        logout(client)
        response = client.get(
                        url_for("admin_bp.new_invite"),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- login_to_continue -->' in response.data.decode()

        login(client, user_creds['editor'])
        response = client.get(
                        url_for("admin_bp.new_invite"),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- my_forms_page -->' in response.data.decode()

        login(client, user_creds['admin'])

    def test_create_invite_without_LINK_variable(self, client):
        response = client.post(
                        url_for("admin_bp.new_invite"),
                        data={
                            "message": "Hello",
                            "email": user_creds['invitee']['email'],
                            "role": DEFAULT_ROLE,
                            "language": self.default_language
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- new_invite_page -->' in html
        assert html.count('ds-error-message') == 1
        assert Invite.find(email=user_creds['invitee']['email']) is None

    def test_create_delete_invitation(self, client):
        """Test invite new user
                permissions
                delete invite
        """
        response = client.post(
                        url_for("admin_bp.new_invite"),
                        data={
                            "message": "Hello [LINK]",
                            "email": user_creds['invitee']['email'],
                            "role": DEFAULT_ROLE,
                            "language": self.default_language
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        if os.environ['SKIP_EMAILS'] == "False":
            assert 'Recipient address rejected:' in html
        else:
            assert 'alert-success' in html
        assert '<!-- list_site_invites_page -->' in html
        new_invite = Invite.find(email=user_creds['invitee']['email'])
        assert new_invite.role == DEFAULT_ROLE
        response = client.get(
                        url_for('admin_bp.delete_invite',
                                invite_id=new_invite.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- list_site_invites_page -->' in response.data.decode()
        assert Invite.find_all().count() == 0

    def test_invite_existing_user(self, client):
        """Invite cannot be created when the user already exists."""
        response = client.post(
                        url_for("admin_bp.new_invite"),
                        data={
                            "message": "Hello [LINK]",
                            "email": user_creds['editor']['email'],
                            "role": self.invitee_role,
                            "language": self.default_language
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- new_invite_page -->' in html
        assert html.count('ds-error-message') == 1

    def test_create_invitation_1(self, client):
        response = client.post(
                        url_for("admin_bp.new_invite"),
                        data={
                            "message": "Hello [LINK]",
                            "email": user_creds['invitee']['email'],
                            "role": self.invitee_role,
                            "language": self.default_language
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200

        new_invite = Invite.find(email=user_creds['invitee']['email'])
        self.properties["invite"] = new_invite
        assert self.invitee_role == new_invite.role
        assert user_creds['invitee']['email'] == new_invite.email

    def test_create_existing_invitation(self, client):
        response = client.post(
                        url_for("admin_bp.new_invite"),
                        data={
                            "message": "Hello [LINK]",
                            "email": user_creds['invitee']['email'],
                            "role": self.invitee_role,
                            "language": self.default_language
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- new_invite_page -->' in response.data.decode()

    def test_consume_invitation_with_unidentified_email(self, client):
        assert self.properties["invite"].token["token"]
        unidentified_email = utils.get_unique_email()
        logout(client)
        response = client.post(
                        url_for('user_bp.create_new_user',
                                invite=self.properties["invite"].token["token"]),
                        data={
                            "username": utils.get_unique_username(),
                            "email": unidentified_email,
                            "password": VALID_PASSWORD,
                            "password2": VALID_PASSWORD,
                            "termsAndConditions": True,
                        },
                        follow_redirects=True,
                    )
        assert '<!-- unvalidated_user_page -->' in response.data.decode()
        user = User.find(email=unidentified_email)
        assert user.validated_email is False

    def test_create_invitation_2(self, client):
        login(client, user_creds['admin'])
        response = client.post(
                        url_for("admin_bp.new_invite"),
                        data={
                            "message": "Hello [LINK]",
                            "email": user_creds['invitee']['email'],
                            "role": self.invitee_role,
                            "language": self.default_language
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200

        new_invite = Invite.find(email=user_creds['invitee']['email'])
        self.properties["invite"] = new_invite
        assert self.invitee_role == new_invite.role
        assert user_creds['invitee']['email'] == new_invite.email

    def test_consume_invitation(self, client):
        assert User.find(email=user_creds['invitee']['email']) is None
        logout(client)
        response = client.post(
                        url_for('user_bp.create_new_user',
                                invite=self.properties["invite"].token["token"]),
                        data={
                            "username": "thebigbadwolf",
                            "email": user_creds['invitee']['email'],
                            "password": VALID_PASSWORD,
                            "password2": VALID_PASSWORD,
                            "termsAndConditions": True,
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- user_settings_page -->' in response.data.decode()
        user = User.find(email=user_creds['invitee']['email'])
        assert user.validated_email is True
        assert user.role == self.invitee_role
        assert Invite.find(id=self.properties["invite"].id) is None

        # delete invitee user to continue testing
        user.delete()
        assert User.find(id=user.id) is None

    def test_create_new_admin_invitation(self, client):
        """Test token creation for new admin user."""
        login(client, user_creds['admin'])
        response = client.post(
                        url_for("admin_bp.new_invite"),
                        data={
                            "message": "Hello [LINK]",
                            "email": user_creds['invitee']['email'],
                            "role": "admin",
                            "language": self.default_language
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        if os.environ['SKIP_EMAILS'] == "False":
            assert 'Recipient address rejected' in html
        else:
            assert 'alert-success' in html
        assert '<!-- list_site_invites_page -->' in html
        new_invite = Invite.find(email=user_creds['invitee']['email'])
        self.properties["invite"] = new_invite
        assert new_invite.role == "admin"

    def test_consume_admin_invitation(self, client):
        assert User.find(email=user_creds['invitee']['email']) is None
        logout(client)
        response = client.post(
                        url_for('user_bp.create_new_user',
                                invite=self.properties["invite"].token["token"]),
                        data={
                            "username": "petertheman",
                            "email": user_creds['invitee']['email'],
                            "password": VALID_PASSWORD,
                            "password2": VALID_PASSWORD,
                            "termsAndConditions": True,
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        user = User.find(email=user_creds['invitee']['email'])
        assert user.validated_email is True
        assert user.role == "admin"
        assert Invite.find_all().count() == 0
        assert '<!-- user_settings_page -->' in response.data.decode()
