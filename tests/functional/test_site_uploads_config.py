"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
from flask import url_for
from liberaforms.models.site import Site
from tests import user_creds
from tests.utils import login, logout


class TestSiteUploadsConfig():

    @classmethod
    def setup_class(cls):
        cls.site = Site.find()

    def test_uploads_tree(self, app):
        assert os.path.isdir(os.path.join(app.config['UPLOADS_DIR'],
                                          app.config['MEDIA_DIR'])) is True
        assert os.path.isdir(os.path.join(app.config['UPLOADS_DIR'],
                                          app.config['BRAND_DIR'])) is True
        assert os.path.isdir(os.path.join(app.config['UPLOADS_DIR'],
                                          app.config['ATTACHMENT_DIR'])) is True
        if 'FQDN' in os.environ:
            assert os.environ['FQDN'] in app.config['MEDIA_DIR']
            assert os.environ['FQDN'] in app.config['BRAND_DIR']
            assert os.environ['FQDN'] in app.config['ATTACHMENT_DIR']

    def test_auth(self, editor, client):
        """Test site_bp.edit_mimetypes"""
        logout(client)
        response = client.get(
                        url_for('site_bp.edit_mimetypes'),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- login_to_continue -->' in response.data.decode()

        logout(client)
        login(client, user_creds['editor'])
        response = client.get(
                        url_for('site_bp.edit_mimetypes'),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- my_forms_page -->' in response.data.decode()

        logout(client)
        login(client, user_creds['admin'])
        response = client.get(
                        url_for('site_bp.edit_mimetypes'),
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert '<!-- edit_mimetypes_page -->' in response.data.decode()

    def test_mimetypes(self, client):
        """Set allowed mimetypes."""
        assert 'jpg' not in self.site.mimetypes['extensions']
        mimetype_data = self.site.mimetypes['extensions']
        mimetype_data.append('jpg')
        mimetype_data = '\n'.join(mimetype_data)
        response = client.post(
                        url_for('site_bp.edit_mimetypes'),
                        data={
                            'extensions': mimetype_data,
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert 'image/jpeg' in self.site.mimetypes['mimetypes']
        assert 'jpg' in self.site.mimetypes['extensions']
