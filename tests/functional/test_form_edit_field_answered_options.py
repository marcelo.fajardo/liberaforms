"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import json
import pytest
from flask import url_for
from liberaforms.models.form import Form
from tests.utils import login, random_slug
from tests import user_creds


class TestEditAnsweredFieldOptions():
    """Given an answered form
       Change multi-option field labels
       Test for unchanged answered multi-option field values."""

    def setup_class(cls):
        cls.properties = {}

    def test_requirements(self, client):

        login(client, user_creds["editor"])

        slug = random_slug()
        form_name = "Answered field edition test form"
        client.post(
            url_for("form_bp.create_new_form"),
            data={
                "name": form_name,
                "slug": slug,
            },
            follow_redirects=True,
        )
        form = Form.find(name=form_name)
        assert form.slug == slug
        self.properties['form'] = form
        assert len(form.structure) == 0
        structure = [
          {
            "inline": False,
            "label": "Checkbox group",
            "name": "checkbox-group-1692872324074",
            "other": False,
            "required": False,
            "type": "checkbox-group",
            "values": [
              {
                "label": "checkbox option 1",
                "value": ""
              },
              {
                "label": "checkbox option 2",
                "value": ""
              },
              {
                "label": "checkbox option 3",
                "value": ""
              },
              {
                "label": "checkbox option 4",
                "value": ""
              }
            ]
          }
        ]
        form.start_edit_mode()
        client.post(
            url_for('form_bp.save_form', form_id=form.id),
            follow_redirects=True,
            data={"structure": json.dumps(structure)}
        )
        assert len(form.structure) == 1
        checkbox_field = form.structure[0]
        assert len(checkbox_field["values"]) == 4
        assert checkbox_field["values"][0]["label"] == "checkbox option 1"
        assert checkbox_field["values"][0]["value"] == "checkbox-option-1"
        assert checkbox_field["values"][1]["label"] == "checkbox option 2"
        assert checkbox_field["values"][1]["value"] == "checkbox-option-2"
        assert checkbox_field["values"][2]["label"] == "checkbox option 3"
        assert checkbox_field["values"][2]["value"] == "checkbox-option-3"
        assert checkbox_field["values"][3]["label"] == "checkbox option 4"
        assert checkbox_field["values"][3]["value"] == "checkbox-option-4"


    def test_post_answer_1(self, client):
        form = self.properties['form']
        form.enabled = True
        form.save()
        assert form.is_public()
        checkbox_field = form.structure[0]
        assert checkbox_field["name"] == "checkbox-group-1692872324074"
        assert checkbox_field["values"][0]["value"] == "checkbox-option-1"
        assert checkbox_field["values"][1]["value"] == "checkbox-option-2"
        # submits checkbox_field["values"][0] aka "checkbox option 1"
        #     and checkbox_field["values"][1] aka "checkbox option 2"
        answer_data = {
            f'{checkbox_field["name"]}[]': [checkbox_field["values"][0]["value"],
                                            checkbox_field["values"][1]["value"]]
        }
        client.post(
            form.url,
            data=answer_data,
            follow_redirects=True,
        )
        assert form.answers.count() == 1
        data = {checkbox_field["name"]:
                f'{checkbox_field["values"][0]["value"]}, {checkbox_field["values"][1]["value"]}'}
        assert form.answers[0].data == data

    def test_edit_answered_field_options_1(self, client):
        form = self.properties['form']
        structure = form.structure.copy()

        assert structure[0]["values"][0]["value"] == "checkbox-option-1"
        assert structure[0]["values"][1]["value"] == "checkbox-option-2"

        edited_checkbox_field = {
          "inline": False,
          "label": "Checkbox group",
          "name": "checkbox-group-1692872324074",
          "other": False,
          "required": False,
          "type": "checkbox-group",
          # changes made:
          # values[0]["label"] is changed
          # values[1]["label"] is changed
          # values[2]["label"] is changed
          "values": [
            {
              "label": "checkbox option 1 edited",
              "value": structure[0]["values"][0]["value"]
            },
            {
              "label": "checkbox option 2 edited",
              "value": structure[0]["values"][1]["value"]
            },
            {
              "label": "checkbox option 3 edited",
              "value": structure[0]["values"][2]["value"]
            },
            {
              "label": "checkbox option 4",
              "value": structure[0]["values"][3]["value"]
            }
          ]
        }
        structure[0] = edited_checkbox_field

        login(client, user_creds["editor"])
        form.start_edit_mode()
        assert not form.is_public()
        assert form.answers.count() == 1
        client.post(
            url_for('form_bp.save_form', form_id=form.id),
            follow_redirects=True,
            data={"structure": json.dumps(structure)}
        )
        assert len(form.structure) == 1
        checkbox_field = form.structure[0]
        assert len(checkbox_field["values"]) == 4

        assert checkbox_field["values"][0]["value"] == "checkbox-option-1"
        assert checkbox_field["values"][1]["value"] == "checkbox-option-2"
        assert checkbox_field["values"][2]["value"] == "checkbox-option-3-edited"
        assert checkbox_field["values"][3]["value"] == "checkbox-option-4_1"

    @pytest.mark.skip(reason="Not implemented")
    def test_delete_answered_field_option(self, client):
        """Deleted filed option data is lost."""
