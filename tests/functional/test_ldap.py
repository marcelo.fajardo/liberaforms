"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import pytest
from flask import url_for
import flask_login
from liberaforms.models.user import User
from liberaforms.models.invite import Invite
from liberaforms.models.formuser import FormUser
from tests import ldap_users, user_creds
from tests import DEFAULT_LANGUAGE
from tests.utils import login, logout, random_slug
from factories import FormFactory



class TestLDAP():
    """Test LDAP."""

    def setup_class(cls):
        cls.properties = {}

    @pytest.mark.skipif(os.environ['ENABLE_LDAP'] != "True", reason="ENABLE_LDAP=False in test.ini")
    def test_requirements(self, editor):
        form = FormFactory(author=editor, slug=random_slug())
        form.save()
        FormUser(user=editor, form=form, is_editor=True).save()
        self.properties['form'] = form

    @pytest.mark.skipif(os.environ['ENABLE_LDAP'] != "True", reason="ENABLE_LDAP=False in test.ini")
    def test_new_user_login(self, client):
        """Given ldap uid/password then create a User in the database and login."""
        new_user = ldap_users[0]
        logout(client)
        response = client.post(
                        url_for('user_bp.login'),
                        data={
                            "username": new_user['username'],
                            "password": new_user['password'],
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        user = User.find(username=new_user['username'])
        assert user.ldap_uuid is not None
        assert flask_login.current_user.username == user.username
        assert "<!-- my_forms_page -->" in response.data.decode()

    @pytest.mark.skipif(os.environ['ENABLE_LDAP'] != "True", reason="ENABLE_LDAP=False in test.ini")
    def test_new_user_login_with_email(self, client):
        """Given ldap uid/mail then create a User in the database and login."""
        new_user = ldap_users[1]
        response = client.post(
                        url_for('user_bp.login'),
                        data={
                            "username": new_user['email'],
                            "password": new_user['password'],
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        user = User.find(email=new_user['email'])
        assert flask_login.current_user.username == user.username
        assert "<!-- my_forms_page -->" in response.data.decode()

    @pytest.mark.skipif(os.environ['ENABLE_LDAP'] != "True", reason="ENABLE_LDAP=False in test.ini")
    def test_login_ldap_user(self, client):
        ldap_user = ldap_users[0]
        user = User.find(username=ldap_user['username'])
        assert user.ldap_uuid is not None
        client.post(
                    url_for('user_bp.login'),
                    data={
                        "username": ldap_user['username'],
                        "password": ldap_user['password'],
                    },
                    follow_redirects=True,
                )
        assert flask_login.current_user.username == ldap_user['username']

    @pytest.mark.skipif(os.environ['ENABLE_LDAP'] != "True", reason="ENABLE_LDAP=False in test.ini")
    def test_form_grant_invite(self, client):
        form = self.properties['form']
        ldap_user = ldap_users[2]
        assert not User.find(username=ldap_user['username'])
        logout(client)
        login(client, user_creds['editor'])
        response = client.post(
                        url_for('form_bp.new_invite',
                                email=ldap_user['email'],
                                form_id=form.id),
                        data={
                            "granted_form_id": form.id,
                            "role": "guest",
                            "email": ldap_user['email'],
                            "message": "Hello [LINK]",
                            "language": DEFAULT_LANGUAGE
                        },
                        follow_redirects=True,
                    )
        invite = Invite.find(email=ldap_user['email'])
        assert invite.ldap_uuid is not None
        assert invite.granted_form['id'] == form.id
        assert invite.email == ldap_user['email']
        assert "<!-- form_readers_page -->" in response.data.decode()
        self.properties['invite'] = invite

    @pytest.mark.skipif(os.environ['ENABLE_LDAP'] != "True", reason="ENABLE_LDAP=False in test.ini")
    def test_accept_form_grant_invite(self, client):
        form = self.properties['form']
        invite = self.properties['invite']
        ldap_user = ldap_users[2]
        assert not User.find(email=ldap_user['email'])
        logout(client)
        response = client.get(
                        url_for('user_bp.create_new_user', invite=invite.token['token']),
                        follow_redirects=True,
                    )
        assert not User.find(username=ldap_user['username'])
        html = response.data.decode()
        # invitation to ldap users are redirected to login page
        assert "<!-- user_login_page -->" in html
        assert invite.token['token'] in html
        response = client.post(
                        url_for('user_bp.login', invite=invite.token['token']),
                        data={
                            "username": ldap_user['username'],
                            "password": ldap_user['password'],
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert "<!-- my_forms_page -->" in response.data.decode()
        user = User.find(username=ldap_user['username'])
        assert user.ldap_uuid is not None
        assert user.validated_email is True
        assert flask_login.current_user.username == user.username
        assert not Invite.find(token=invite.token['token'])
        assert FormUser.find(form_id=form.id, user_id=user.id, is_editor=False)
