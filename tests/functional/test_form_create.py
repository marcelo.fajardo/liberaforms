"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import pytest
import json
from flask import g, current_app, url_for
from liberaforms.models.form import Form
from liberaforms.models.formuser import FormUser
from liberaforms.utils import validators
from tests.utils import login, logout, random_slug, get_form_structure
from tests import user_creds
import flask_login


class TestCreateForm():

    def test_auth(self, client):
        """Test form_bp.create_new_form"""
        logout(client)
        response = client.get(
                        url_for("form_bp.create_new_form"),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- login_to_continue -->' in response.data.decode()

        login(client, user_creds['editor'])

    def test_create_form(self, client):
        """Creates a form then tests properties."""
        response = client.get(
                        url_for("form_bp.create_new_form"),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- new_form_page -->' in response.data.decode()
        slug = random_slug()
        form_name = "A new form"
        response = client.post(
                        url_for("form_bp.create_new_form"),
                        data={
                            "name": form_name,
                            "slug": slug,
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- form_configuration_page -->' in response.data.decode()
        form = Form.find(slug=slug)
        assert f"<h1>{form_name}</h1>" in form.introduction_text["html"]
        assert f"# {form_name}" in form.introduction_text["markdown"]
        assert form_name in form.introduction_text['short_text']
        assert form.author_id == flask_login.current_user.id
        assert form.enabled is False
        assert form.users.count() == 1
        assert form.log.count() == 1
        assert FormUser.find(form_id=form.id,
                             user_id=flask_login.current_user.id,
                             is_editor=True)



class TestSlugAvailability():

    def test_auth(self, client):
        logout(client)
        response = client.post(
                        url_for('form_bp.is_slug_available'),
                        follow_redirects=False,
                    )
        assert response.status_code == 401
        assert response.is_json is True

        login(client, user_creds['editor'])

    def test_slug_availability(self, client):
        reserved_slug = current_app.config['RESERVED_SLUGS'][0]

        response = client.post(
                        url_for('form_bp.is_slug_available'),
                        data={
                            "slug": reserved_slug,
                        },
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert response.is_json is True
        assert response.json['available'] is False
        unavailable_slug = Form.find().slug
        response = client.post(
                        url_for('form_bp.is_slug_available'),
                        data={
                            "slug": unavailable_slug,
                        },
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert response.is_json is True
        assert response.json['available'] is False

    # def test_preview_save_form_with_unavailable_slug(self,client):
    #     unavailable_slug = Form.find().slug
    #     response = editor_client.post(
    #                     "/form/edit",
    #                     data = {
    #                         "structure": get_form_structure(),
    #                         "introductionTextMD": "hello",
    #                         "slug": unavailable_slug,
    #                     },
    #                     follow_redirects=False,
    #                 )
    #     assert response.status_code == 302
    #     html = response.data.decode()
    #     assert '/form/edit</a>' in html
    #     response = editor_client.post(
    #                     "/form/save",
    #                     data = {
    #                         "structure": get_form_structure(),
    #                         "introductionTextMD": "hello",
    #                         "slug": unavailable_slug,
    #                     },
    #                     follow_redirects=False,
    #                 )
    #     assert response.status_code == 302
    #     html = response.data.decode()
    #     assert '/form/edit</a>' in html

    # def test_preview_save_form_with_reserved_slug(self, editor_client):
    #     reserved_slug = current_app.config['RESERVED_SLUGS'][0]
    #     response = editor_client.post(
    #                     "/form/edit",
    #                     data = {
    #                         "structure": get_form_structure(),
    #                         "introductionTextMD": "hello",
    #                         "slug": reserved_slug,
    #                     },
    #                     follow_redirects=False,
    #                 )
    #     assert response.status_code == 302
    #     html = response.data.decode()
    #     assert '/form/edit</a>' in html
    #     #assert '<form action="/form/save" method="post">' in html
    #     response = editor_client.post(
    #                     "/form/save",
    #                     data = {
    #                         "structure": get_form_structure(),
    #                         "introductionTextMD": "hello",
    #                         "slug": reserved_slug,
    #                     },
    #                     follow_redirects=False,
    #                 )
    #     assert response.status_code == 302
    #     html = response.data.decode()
    #     assert '/form/edit</a>' in html
