"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import pytest
import json
from flask import url_for
import flask_login
from liberaforms.models.site import Site
from liberaforms.models.consent import Consent
from tests.factories import ConsentFactory
from tests.factories import FormFactory
from tests import user_creds
from tests import utils
from tests.utils import login, logout


class TestUserConsent():
    """Test List user consents
            Create a user data consent
            Edit consent
            Delete consent."""

    @classmethod
    def setup_class(cls):
        cls.properties = {}
        cls.site = Site.find()

    def test_requirements(self, editor):
        pass

    def test_auth(self, editor, client):
        """Test consent_bp.list_user_data_consents
                consent_bp.edit_user_data_consent
                consent_bp.delete_user_data_consent."""

        consent = ConsentFactory(user_id=editor.id)
        consent.save()

        logout(client)
        response = client.get(
                        url_for('consent_bp.list_user_data_consents'),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- login_to_continue -->' in response.data.decode()
        response = client.get(
                        url_for('consent_bp.edit_user_data_consent', consent_id=consent.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- login_to_continue -->' in response.data.decode()
        response = client.post(
                        url_for('consent_bp.delete_user_data_consent'),
                        data={
                            "consent_id": consent.id,
                        },
                        follow_redirects=False,
                    )
        assert response.status_code == 401

        login(client, user_creds["admin"])
        response = client.get(
                        url_for('consent_bp.edit_user_data_consent', consent_id=consent.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- list_user_consents_page -->' in response.data.decode()
        response = client.post(
                        url_for('consent_bp.delete_user_data_consent'),
                        data={
                            "consent_id": consent.id,
                        },
                        follow_redirects=False,
                    )
        assert response.status_code == 406

    def test_library(self, editor, client):
        login(client, user_creds["editor"])
        response = client.get(
                        url_for('consent_bp.list_user_data_consents'),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- list_user_consents_page -->' in response.data.decode()

    def test_new_consent(self, editor, client):
        response = client.get(
                        url_for('consent_bp.edit_user_data_consent'),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- user_edit_consent_page -->' in response.data.decode()
        initial_cnt = Consent.find_all(user_id=editor.id).count()
        response = client.post(
                        url_for('consent_bp.edit_user_data_consent'),
                        follow_redirects=False,
                        data={
                            "name": "name",
                            "language_selector": self.site.language,
                            "language": editor.preferences["language"],
                            "label": "label",
                            "md_text": "md_text",
                            "required": True,
                            "wizard": ""
                        }
                    )
        assert response.status_code == 200
        assert '<!-- user_edit_consent_page -->' in response.data.decode()
        assert Consent.find_all(user_id=editor.id).count() == initial_cnt + 1
        consent = Consent.find(user_id=editor.id)
        self.properties["consent"] = consent

    def test_edit_consent(self, editor, client):
        consent = self.properties["consent"]
        new_name = "new user consent name"
        response = client.post(
                        url_for('consent_bp.edit_user_data_consent',
                                consent_id=consent.id),
                        follow_redirects=False,
                        data={
                            "name": new_name,
                            "language_selector": self.site.language,
                            "language": editor.preferences["language"],
                            "label": "A label",
                            "md_text": "# Hello",
                            "required": True,
                            "wizard": ""
                        }
                    )
        assert response.status_code == 200
        assert '<!-- user_edit_consent_page -->' in response.data.decode()
        assert consent.name == new_name

    def test_delete_consent(self, client):
        consent = self.properties["consent"]
        response = client.post(
                        url_for('consent_bp.delete_user_data_consent'),
                        data={
                            "consent_id": consent.id,
                        },
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert response.is_json is True
        assert response.json['deleted'] is True
        assert not Consent.find(id=consent.id)
