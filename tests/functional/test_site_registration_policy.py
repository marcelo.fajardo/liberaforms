"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

from flask import url_for
from liberaforms.models.site import Site
from tests import user_creds
from tests.utils import login, logout

class TestSiteRegistrationPolicy():

    @classmethod
    def setup_class(cls):
        cls.site = Site.find()

    def test_auth(self, client):
        """Test site_bp.toggle_invitation_only
                site_bp.toggle_newuser_uploads_default"""
        logout(client)
        response = client.post(
                        url_for('site_bp.toggle_invitation_only'),
                        follow_redirects=True,
                    )
        assert response.status_code == 401
        response = client.post(
                        url_for('site_bp.toggle_newuser_uploads_default'),
                        follow_redirects=True,
                    )
        assert response.status_code == 401

        logout(client)
        login(client, user_creds['editor'])
        response = client.post(
                        url_for('site_bp.toggle_invitation_only'),
                        follow_redirects=True,
                    )
        assert response.status_code == 401
        response = client.post(
                        url_for('site_bp.toggle_newuser_uploads_default'),
                        follow_redirects=True,
                    )
        assert response.status_code == 401

        logout(client)
        login(client, user_creds['admin'])

    def test_toggle_invitation_only(self, client):
        invitation_only = self.site.invitation_only
        response = client.post(
                        url_for('site_bp.toggle_invitation_only'),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert response.is_json is True
        assert self.site.invitation_only != invitation_only

    def test_toggle_newuser_enableuploads(self, client):
        initial_newuser_enableuploads = self.site.newuser_enableuploads
        response = client.post(
                        url_for('site_bp.toggle_newuser_uploads_default'),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert response.is_json is True
        assert response.json == {"uploads": self.site.newuser_enableuploads}
        assert initial_newuser_enableuploads != self.site.newuser_enableuploads
