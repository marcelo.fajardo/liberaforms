"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import json
from urllib.parse import quote_plus as urllib_quote_p
from flask import url_for
import flask_login
from liberaforms.models.formuser import FormUser
from tests.factories import FormFactory, UserFactory
from tests.utils import login, logout
from tests import utils
from tests import VALID_PASSWORD, user_creds


class TestLoginLogout():

    def test_user_login_logout_test_utils(self, client):
        assert not flask_login.current_user
        login(client, user_creds['editor'])
        assert not flask_login.current_user.is_anonymous
        assert flask_login.current_user.is_authenticated
        assert flask_login.current_user.username == user_creds["editor"]["username"]
        logout(client)
        assert flask_login.current_user.is_anonymous
        assert not flask_login.current_user.is_authenticated

    def test_anonymous_user_mixin(self, client):
        assert not flask_login.current_user
        response = client.get(
                        url_for('main_bp.index'),
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert flask_login.current_user.is_anonymous
        assert not flask_login.current_user.is_authenticated
        assert not flask_login.current_user.is_active
        assert flask_login.current_user.get_id() is None

        response = client.post(
                        url_for('user_bp.login'),
                        data={
                            "username": user_creds["editor"]["username"],
                            "password": user_creds["editor"]["password"]
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert "<!-- my_forms_page -->" in response.data.decode()
        assert flask_login.current_user.__class__.__name__ == "User"
        assert not flask_login.current_user.is_anonymous
        assert flask_login.current_user.is_authenticated
        assert flask_login.current_user.is_active() is True
        assert flask_login.current_user.get_id() is flask_login.current_user.id


    def test_current_user(self, client, editor):
        assert not flask_login.current_user

        login(client, user_creds['editor'])
        assert flask_login.current_user.is_authenticated
        assert flask_login.current_user.username == user_creds["editor"]["username"]
        response = client.get(
                        url_for('user_bp.login'),
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert "<!-- user_login_page -->" in response.data.decode()
        assert flask_login.current_user.is_anonymous
        assert not flask_login.current_user.is_authenticated

        response = client.get(
                        url_for('user_bp.logout'),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert "<!-- site_index_page -->" in response.data.decode()

        response = client.post(
                        url_for('user_bp.login'),
                        data={
                            "username": user_creds["editor"]["username"],
                            "password": user_creds["editor"]["password"]
                        },
                        follow_redirects=True,
                    )
        assert "<!-- my_forms_page -->" in response.data.decode()
        assert not flask_login.current_user.is_anonymous
        assert flask_login.current_user.is_authenticated
        assert flask_login.current_user.id == editor.id

        response = client.get(
                        url_for('user_bp.logout'),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert "<!-- site_index_page -->" in response.data.decode()
        assert flask_login.current_user.is_anonymous
        assert not flask_login.current_user.is_authenticated


    def test_invalid_login(cls, client):
        response = client.post(
                        url_for('user_bp.login'),
                        data={
                            "username": "",
                            "password": "",
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- user_login_page -->' in response.data.decode()
        assert not flask_login.current_user.is_authenticated

    def test_required_login_url(self, client, editor):
        form = FormFactory(author=editor,
                           slug=utils.random_slug(),
                           structure=json.loads(utils.get_form_structure()))
        form.save()
        FormUser(form=form, user=editor, is_editor=True).save()
        logout(client)
        required_login_url = url_for('answers_bp.list', form_id=form.id)
        response = client.get(
                        required_login_url,
                        follow_redirects=False,
                    )
        assert response.status_code == 302
        redirect_url = f"/user/login?next={urllib_quote_p(required_login_url)}"
        assert redirect_url in response.data.decode()

    def test_blocked_user_login(self, client):
        user = UserFactory(validated_email=True, role="editor")
        user.blocked = True
        user.save()
        response = client.post(
                        url_for('user_bp.login'),
                        data={
                            "username": user.username,
                            "password": VALID_PASSWORD
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- user_login_page -->' in response.data.decode()
        assert not flask_login.current_user.is_authenticated

    def test_valid_login(self, admin, users, client):
        response = client.post(
                        url_for('user_bp.login'),
                        data={
                            "username": users['admin']['username'],
                            "password": users['admin']['password'],
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert "<!-- my_forms_page -->" in response.data.decode()
        assert flask_login.current_user.is_authenticated
        assert flask_login.current_user.username == admin.username

    def test_logout(self, client):
        response = client.get(
                        url_for('user_bp.logout'),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- site_index_page -->' in response.data.decode()
        assert not flask_login.current_user.is_authenticated
