"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
from io import BytesIO
import pytest
import werkzeug
from flask import url_for
from liberaforms.models.site import Site
from tests.utils import login, logout
from tests import user_creds
import flask_login


class TestSiteLookAndFeel():

    @classmethod
    def setup_class(cls):
        cls.site = Site.find()

    def test_auth(self, editor, client):
        """Test site_bp.look_and_feel
                site_bp.change_name
                site_bp.change_icon."""
        logout(client)
        response = client.get(
                        url_for('site_bp.look_and_feel'),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- login_to_continue -->' in response.data.decode()
        response = client.post(
                        url_for('site_bp.change_name'),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- login_to_continue -->' in response.data.decode()
        response = client.post(
                        url_for('site_bp.change_icon'),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- login_to_continue -->' in response.data.decode()
        response = client.get(
                        url_for('site_bp.reset_icon'),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- login_to_continue -->' in response.data.decode()
        response = client.post(
                        url_for('site_bp.change_theme'),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- login_to_continue -->' in response.data.decode()

        login(client, user_creds['editor'])
        response = client.get(
                        url_for('site_bp.look_and_feel'),
                        follow_redirects=True,
                    )
        assert '<!-- my_forms_page -->' in response.data.decode()
        response = client.post(
                        url_for('site_bp.change_name'),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- my_forms_page -->' in response.data.decode()
        response = client.post(
                        url_for('site_bp.change_icon'),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- my_forms_page -->' in response.data.decode()
        response = client.get(
                        url_for('site_bp.reset_icon'),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- my_forms_page -->' in response.data.decode()
        response = client.post(
                        url_for('site_bp.change_theme'),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- my_forms_page -->' in response.data.decode()

        logout(client)
        login(client, user_creds['admin'])

    def test_change_sitename(self, client):
        response = client.post(
                        url_for('site_bp.change_name'),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- site_look_and_feel_page -->' in response.data.decode()
        initial_sitename = self.site.name
        new_name = "New name!!"
        response = client.post(
                        url_for('site_bp.change_name'),
                        data={
                            "sitename": new_name,
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- site_look_and_feel_page -->' in response.data.decode()
        assert self.site.name != initial_sitename

    def test_change_logo(self, app, client):
        """ Tests valid and invalid image files in ./tests/assets
            Tests jpeg to png logo conversion
            Tests favicon creation
        """
        brand_dir = os.path.join(app.config['UPLOADS_DIR'],
                                 app.config['BRAND_DIR'])
        logo_path = os.path.join(brand_dir, 'logo.png')
        favicon_path = os.path.join(brand_dir, 'favicon.ico')
        initial_logo_stats = os.stat(logo_path)
        initial_favicon_stats = os.stat(favicon_path)
        invalid_logo = "invalid_logo.jpeg"
        #invalid_favicon = "favicon_invalid.jpeg"
        with open(f'./assets/{invalid_logo}', 'rb') as f:
            stream = BytesIO(f.read())
        invalid_file = werkzeug.datastructures.FileStorage(
            stream=stream,
            filename=invalid_logo,
            content_type="plain/txt",
        )
        response = client.post(
                    url_for('site_bp.change_icon'),
                    data={
                        'file': invalid_file,
                    },
                    follow_redirects=True,
                    content_type='multipart/form-data',
                )
        assert response.status_code == 200
        assert initial_logo_stats.st_size == os.stat(logo_path).st_size
        assert initial_favicon_stats.st_size == os.stat(favicon_path).st_size
        assert '<!-- site_look_and_feel_page -->' in response.data.decode()
        valid_logo = "valid_logo.jpeg"
        with open(f'./assets/{valid_logo}', 'rb') as f:
            stream = BytesIO(f.read())
        valid_file = werkzeug.datastructures.FileStorage(
            stream=stream,
            filename=valid_logo,
            content_type="image/png",
        )
        response = client.post(
                    url_for('site_bp.change_icon'),
                    data={
                        'file': valid_file,
                    },
                    follow_redirects=True,
                    content_type='multipart/form-data',
                )
        assert response.status_code == 200
        assert initial_logo_stats.st_size != os.stat(logo_path).st_size
        assert initial_favicon_stats.st_size != os.stat(favicon_path).st_size
        assert '<!-- site_look_and_feel_page -->' in response.data.decode()

    def test_restore_default_logo(self, app, client):
        brand_dir = os.path.join(app.config['UPLOADS_DIR'],
                                 app.config['BRAND_DIR'])
        logo_path = os.path.join(brand_dir, 'logo.png')
        default_logo_path = os.path.join(brand_dir, 'logo-default.png')
        favicon_path = os.path.join(brand_dir, 'favicon.ico')
        default_favicon_path = os.path.join(brand_dir, 'favicon-default.ico')
        initial_logo_stats = os.stat(logo_path)
        initial_favicon_stats = os.stat(favicon_path)
        response = client.get(
                        url_for('site_bp.reset_icon'),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert initial_logo_stats.st_size != os.stat(logo_path).st_size
        assert os.stat(logo_path).st_size == os.stat(default_logo_path).st_size
        assert initial_favicon_stats.st_size != os.stat(favicon_path).st_size
        assert os.stat(favicon_path).st_size == os.stat(default_favicon_path).st_size
        assert '<!-- site_look_and_feel_page -->' in response.data.decode()

    def test_change_theme(self, client):
        """Tests valid and invalid html hex color."""

        # site.theme['navbar']['font_color'] possible colors #ddd or #333
        assert self.site.theme['navbar']['font_color'] == "#ddd"
        response = client.post(
                        url_for('site_bp.change_theme'),
                        data={
                            "font_color": "#333",
                            "primary_color": "#000"
                        },
                        follow_redirects=True,
                    )

        assert response.status_code == 200
        assert self.site.theme['navbar']['font_color'] == "#333"
        assert self.site.theme["primary_color"] == "#000"

        response = client.post(
                        url_for('site_bp.change_theme'),
                        data={
                            "font_color": "not_hex_code",
                            "primary_color": "#999"
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert self.site.theme['navbar']['font_color'] == "#ddd"  # default value
        assert self.site.theme["primary_color"] == "#999"
        assert '<!-- site_look_and_feel_page -->' in response.data.decode()
