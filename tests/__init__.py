"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2022 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import sys
import shutil
from tests import utils

sys.path.append(os.getcwd())  # import our factories module

os.environ['FLASK_CONFIG'] = 'testing'

# setup tests with temporary database
os.environ['DB_USER'] = os.environ['TEST_DB_USER']
os.environ['DB_PASSWORD'] = os.environ['TEST_DB_PASSWORD']
os.environ['DB_HOST'] = os.environ['TEST_DB_HOST']
os.environ['DB_NAME'] = os.environ['TEST_DB_NAME']

# remove temporary directories left behind after the last test
logs_dir = os.path.join(os.getcwd(), "logs")
media_dir = os.path.join(os.getcwd(), "uploads/media/hosts/localhost")
attach_dir = os.path.join(os.getcwd(), "uploads/attachments/hosts/localhost")
shutil.rmtree(logs_dir, ignore_errors=True)
shutil.rmtree(media_dir, ignore_errors=True)
shutil.rmtree(attach_dir, ignore_errors=True)

VALID_PASSWORD = "this is a valid password"
INVALID_PASSWORD = "eeee eeee"

DEFAULT_ROLE = 'editor'
DEFAULT_LANGUAGE = os.environ['DEFAULT_LANGUAGE']

user_creds = {
    "root_user": {
        "username": "tuttle",
        "email":  os.environ['ROOT_USER'],
        "password": VALID_PASSWORD
    },
    "admin": {
        "username": "porru",
        "email":  "porru@example.com",
        "password": VALID_PASSWORD
    },
    "editor": {
        "username": os.environ['EDITOR_USERNAME'],
        "email": os.environ['EDITOR_EMAIL'],
        "password": VALID_PASSWORD
    },
    "invitee": {
        "email": os.environ['INVITEE_EMAIL']
    }
}

ldap_users: dict = utils.get_ldap_users()
