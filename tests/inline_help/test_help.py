"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import json
import re
import pytest
from bs4 import BeautifulSoup
from flask import url_for
from flask_babel import force_locale
from flask_babel import gettext
from tests import user_creds
from tests.utils import login
from liberaforms.models.site import Site
from liberaforms.utils.inline_help import InlineHelp

GREEN = '\033[0;32;48m'
RED = '\033[1;31;48m'
END = '\033[1;37;0m'


class TestPages():
    """."""

    @classmethod
    def setup_class(cls):
        cls.properties = {}

    def test_requirements(self, app, client):
        login(client, user_creds['editor'])
        site_language = Site.find().language
        inline_help = InlineHelp(language=site_language, user_role="editor")
        self.properties["inline_help"] = inline_help
        assert inline_help.help_dir == os.path.join(app.config['ASSETS_DIR'], "inline_help")
        self.properties["page_dir"] = os.path.join(inline_help.help_dir, "pages")
        self.properties["page_files"] = os.listdir(self.properties["page_dir"])
        assert len(self.properties["page_files"])
        self.properties["page_files"].sort()

    def test_pages(self, app, client):
        inline_help = self.properties["inline_help"]
        print()
        login(client, user_creds['editor'])
        for file_name in self.properties["page_files"]:
            page_path = os.path.join(self.properties["page_dir"], file_name)
            try:
                with open(page_path, 'r', encoding="utf-8") as page_file:
                    content = page_file.read()
                translatable_strings = re.findall(r'_\("(.*?)"\)', content)
                for translatable in translatable_strings:
                    for lang_code in app.config["LANGUAGES"].keys():
                        with force_locale(lang_code):
                            translation = gettext(translatable)
                        if translation.startswith(" "):
                            raise ValueError(f"lang: {lang_code}, '{translation}' starts with white space")
                        if translation.endswith(" "):
                            raise ValueError(f"lang: {lang_code}, '{translation}' ends with white space")
                        if translation.count("$$") != translatable.count("$$"):
                            raise ValueError(f"lang: {lang_code}, '$$ $$' mismatch")
                        if translation.count("`") != translatable.count("`"):
                            raise ValueError(f"lang: {lang_code}, code block '`' mismatch")

                if not file_name.startswith("_"):
                    assert inline_help.get_page_label(file_name) != "No page title"

                page_html = inline_help.get_page(file_name)
                soup = BeautifulSoup(page_html, features="lxml")
                linked_files = soup.find_all(attrs={"onclick": True})
                for linked_file in linked_files:
                    onclick_value = linked_file.get("onclick")
                    linked_file_name = re.findall('"([^"]*)"', onclick_value)[0]
                    if linked_file_name not in self.properties["page_files"]:
                        raise ValueError(f"Cannot find {linked_file_name}")
                print(f"{GREEN}PASSED{END} {file_name}")
            except Exception as error:
                print(f"{RED}FAILED{END} {file_name}")
                print(error)
                pytest.exit(f"{RED}Failed: {file_name}, {error}{END}")
