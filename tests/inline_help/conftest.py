"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

#import flask
import pytest


@pytest.fixture(scope='function')
def client(app):
    with app.test_client() as _client:
        yield _client


# @pytest.fixture(scope='function')
# def anon_client(app):
#     return app.test_client()
#     #with app.test_client() as client:
#     #    yield client
#     #while True:
#     #    top = flask._request_ctx_stack.top
#     #    if top is not None and top.preserved:
#     #        top.pop()
#     #    else:
#     #        break

@pytest.fixture(scope='class')
def guest_client(app):
    with app.test_client() as _client:
        yield _client

# @pytest.fixture(scope='class')
# def editor_client(app):
#     with app.test_client() as client:
#         yield client
#
# @pytest.fixture(scope='class')
# def admin_client(app):
#     with app.test_client() as client:
#         yield client

# @pytest.fixture(scope='function')
# def admin_client(app, admin):
#     with app.test_client() as client:
#         with client.session_transaction() as sess:
#             sess['id'] = admin.id
#             sess['_fresh'] = True
#             yield client

#@pytest.fixture(scope="module")
#def invite():
#    return {
#        'id': None,
#        'token': None,
#    }

@pytest.fixture(scope="module")
def expiry_conditions():
    return {
        "number_field_max": 7,
        "max_answers": 10
    }
